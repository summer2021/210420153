#ifndef _XBOOK_MM_H
#define _XBOOK_MM_H

#include <xbook/memspace.h>
#include <xbook/list.h>
#include <xbook/bitops.h>
#include <arch/atomic.h>
#include <arch/page.h>
#include <stddef.h>

/* 页描述符 */
struct page {
    void* mapping;  /* 记录交换高速缓存 */
    struct rev_mapping* rev_mapping;    /* 记录反映射 */
    unsigned long index;    /* 记录在用户空间内的偏移（单位：页） */
    unsigned long swp_entry;    /* 交换项 */
    unsigned long flags;    
    list_t lru;
};

extern struct page* mem_map;

/* 页描述符flags的标志位在flags内的位置 */
enum page_flags {
    PG_lru = 0,
    PG_active,
    PG_referenced,
    PG_reserved,
    PG_dirty,
    PG_launder,
    PG_uptodate,
};

/*
 * 以下宏是设置struct page->flags的标志位
 * 分别是：
 * 1、测试位
 * 2、设置位
 * 3、清除位
 */
#define PageLRU(page)		test_bit(PG_lru, &(page)->flags)
#define ClearPageLRU(page)      clear_bit(PG_lru, &(page)->flags)
#define TestSetPageLRU(page)	test_and_set_bit(PG_lru, &(page)->flags)
#define TestClearPageLRU(page)	test_and_clear_bit(PG_lru, &(page)->flags)

#define PageActive(page)    test_bit(PG_active, &(page)->flags)
#define SetPageActive(page) set_bit(PG_active, &(page)->flags)
#define ClearPageActive(page)   clear_bit(PG_active, &(page)->flags)

#define PageReferenced(page)	test_bit(PG_referenced, &(page)->flags)
#define SetPageReferenced(page)	set_bit(PG_referenced, &(page)->flags)
#define ClearPageReferenced(page)	clear_bit(PG_referenced, &(page)->flags)
#define TestAndClearReferenced(page)    test_and_clear_bit(PG_referenced, &(page)->flags)

#define PageReserved(page)	test_bit(PG_reserved, &(page)->flags)
#define SetPageReserved(page)   set_bit(PG_reserved, &(page)->flags)
#define ClearPageReserved(page)	clear_bit(PG_reserved, &(page)->flags)

#define PageDirty(page)		test_bit(PG_dirty, &(page)->flags)
#define SetPageDirty(page)	set_bit(PG_dirty, &(page)->flags)
#define ClearPageDirty(page)	clear_bit(PG_dirty, &(page)->flags)

#define PageLaunder(page)	test_bit(PG_launder, &(page)->flags)
#define SetPageLaunder(page)	set_bit(PG_launder, &(page)->flags)
#define ClearPageLaunder(page)	clear_bit(PG_launder, &(page)->flags)

#define PageUptodate(page)	test_bit(PG_uptodate, &(page)->flags)
#define SetPageUptodate(page)   set_bit(PG_uptodate, &(page)->flags)
#define ClearPageUptodate(page)	clear_bit(PG_uptodate, &(page)->flags)


#define GetMemMapIndex(pageaddr) (&mem_map[(pageaddr >> PAGE_SHIFT)])
#define GetPfn(page)             ((u32_t)((struct page*)page - mem_map))
#define GetPagePhyAdd(page)      ((u32_t)(GetPfn(page) << PAGE_SHIFT))
#define GetPageFromLru(pnode)    container_of(pnode, struct page, lru)       

static inline void StructPageInit(struct page* page) 
{                         
    page->mapping = NULL;
    page->rev_mapping = NULL;    
    page->index = 0;
    page->swp_entry = 0;
    page->flags = 0;   
    list_init(&page->lru);
}

#endif