#include <sys/syscall.h>
#include <stdlib.h>
#include <stdio.h>

enum errcode {
    /* the file "/swapfile" isn't closed */
    EDFSWP = 15,
    /* the file hasn't been opened */
    ENOPEN,
    /* the file is not swap space */
    ENSWPS,
    /* the swap space is being written */
    EWRITING,
    /* one of seed_swaps in swap_info is in swapout_list */
    EINSWPL,
};

static const char* errdfswp_str = "failed to close the file as the file is /swapfile.";
static const char* erreopen_str = "the file hasn't been opened.";
static const char* errnswps_str = "the file is not swap space.";
static const char* errwriting_str = "the swap space is being written.";
static const char* errswpl_str = "one of seed_swaps in swap_info is in swapout_list";
int main(int argc, char *argv[])
{
    int ret_value;
    const char* str_array[] = {errdfswp_str, erreopen_str, errnswps_str, 
                 errwriting_str, errswpl_str};

    if (argc == 1) {
        fprintf(stderr, "swapoff: arguments is too less.\n");
        return -1;
    }

    if ((ret_value = syscall1(int, SYS_SWAPOFF, argv[1])) >= 0) 
        fprintf(stdout, "swapoff: finish turning off the swap space!\n");
    else 
        fprintf(stdout, "swapoff: %s\n", str_array[abs(ret_value + 15)]);
    return 0;
}