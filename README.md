8/15
改动：
以下三个文件更新了LRU策略
src/arch/x86/mach-i386/mm/page.c 
src/arch/x86/mach-i386/mm/vmm.c 
src/ipc/sharemem.c
以下一个文件改动了range结构体
src/arch/x86/include/arch/mempool.h
以下一个文件新增与range结构体相关的一段代码
src/arch/x86/mach-i386/mm/mempool.c
以下一个文件新增一行代码，为mem_map分配内存
src/arch/x86/mach-i386/mm/phymem.c
以下一个文件新增一行代码，放置初始化内存交换函数接口
src/init/main.c

新增：
在src/include/xbook中新增了.h文件：
mm.h：与页结构体相关，有描述页的结构体以及宏
lru.h：包含lru的函数接口以及一些宏
radix_tree.h：与基树相关的结构体以及一些函数接口
swap.h：与内存交换相关，包含记录交换区状态的结构体、交换高速缓存结构体以及接口
在src/mm中新增了.c文件：lru.c、radix_tree.c、swap.c、swapcache.c、swap_stat.c
swap.c：已实现交换区的初始化，未来将实现内存换出/换入函数
swapcache.c：和交换高速缓存相关的操作都在这里
swap_stat.c：记录页在交换高速缓存的状态的相关函数，不过仅实现一点

9/17
改动：
src/init/main.c：新增基于space反映射的初始化
src/mm/lru.c：由于xbook仅支持单核，把自旋锁改为互斥锁
src/include/xbook/mm.h：改进了结构体struct page，新增了与反映射相关的字段，新增一个内联函数，初始化struct page
src/include/xbook/memspace.h：新增基于space反映射的结构体，以及它的函数接口，结构体mem_space_t新增了与反映射相关的字段
src/vmm/memspace：把基于space反映射的功能融入到space当中。为space添加反映射，为struct设置在space内的偏移（以页为单位）
src/vmm/vmm.c：新增了移除VMM时，删除space和反映射；新增把父进程的space复制到子进程时，设置反映射
src/arch/x86/mach-i386/mm/vmm.c：新增设置struct page在space内的偏移
src/ipc/sharemem.c：优化了大部分代码，新增对红黑树的支持
src/arch/x86/mach-i386/mm/page.c：在处理缺页故障函数新增了新分配的物理页在所属的space内的偏移，优化了小部分代码
src/mm/radix_tree.c：改进radix_tree的节点结构体，改进了节点回收函数，以及在内存池查询空闲节点策略

新增：
src/include/xbook/rb_tree.h：红黑树的根结构体/节点结构体，以及一些函数接口，内联函数
src/mm/rb_tree.c：红黑树的函数，目前用在sharemem.c以及rev_mapping.c
src/rev_mapping.c：基于space反映射的函数

9/19
改动：
在文件系统新增一个函数：根据文件路径在全局文件状态表中查询文件是否被其他任务打开。其中涉及修改的文件有：src/fs/kernif.c, src/include/xbook/fs.h, src/include/xbook/fsal.h, src/fs/fsal/fsalif.c, src/fs/fsal/fatfs.c

给全局文件状态表新增一个获取/释放互斥锁的接口函数。其中涉及修改的文件有：src/fs/faal/file.c, src/include/xbook/file.h

修改了内存交换模块的初始化，最大支持16个交换区，给每个交换区设置一个头部，给每个已打开的交换区设置优先级。

新增：
新增mkswap, swapon, swapoff的系统调用，新增mkswap, swapon, swapoff的命令（用户态程序），在bin目录（swapoff未完成）
新增文件src/mm/swap_file.c：创建交换区sys_mkswap, 打开交换区sys_swapon, 关闭交换区sys_swapoff（还未完成）
