#ifndef _XBOOK_LRU_H
#define _XBOOK_LRU_H

#include <xbook/list.h>
#include <xbook/mm.h>
#include <xbook/debug.h>
#include <arch/mempool.h>

#define DEBUG_LRU_PAGE(page)			    \
do {						                \
	if (!PageLRU(page))			            \
		keprint(PRINT_ERR                   \
        "LRU: page %x has had no LRU\n",    \
        GetPagePhyAdd(page));               \
	if (PageActive(page))			        \
		keprint(PRINT_ERR                   \
        "LRU: page %x is in active_list\n", \
        GetPagePhyAdd(page));               \
} while (0)

#define add_page_to_active_list(range, page)		\
do {						                		\
	DEBUG_LRU_PAGE(page);			        		\
	SetPageActive(page);			        		\
	list_add(&(page)->lru, &(range)->active_list);	\
	(range)->nr_active_pages++;			            \
} while (0)

#define add_page_to_inactive_list(range, page)		\
do {						               			\
	DEBUG_LRU_PAGE(page);			        		\
	list_add(&(page)->lru, &(range)->inactive_list);\
	(range)->nr_inactive_pages++;			        \
} while (0)

#define add_page_to_swapout_list(list, page)		\
do {												\
	list_add_tail(&(page)->lru, list);				\
	SetPageDirty(page);								\
} while (0)

#define del_page_from_active_list(range, page)		\
do {						               			\
	list_del(&(page)->lru);		        			\
	ClearPageActive(page);		        			\
	(range)->nr_active_pages--;		            	\
} while (0)

#define del_page_from_inactive_list(range, page)	\
do {						                		\
	list_del(&(page)->lru);		        			\
	(range)->nr_inactive_pages--;		        	\
} while (0)

#define del_page_from_swapout_list(page)			\
do {												\
	list_del(&(page)->lru);							\
	ClearPageDirty(page);							\
	ClearPageLaunder(page);							\
} while (0)


void lru_cache_add(mem_range_t* range, struct page* page);
void lru_cache_del(mem_range_t* range, struct page* page);
void swapout_del(mem_range_t* range, struct page* page);
void mark_page_accessed(int range_type, struct page* page);

#define lru_add(range, pageaddr) \
		lru_cache_add(range, GetMemMapIndex(pageaddr))

#define lru_del(range, pageaddr) \
		lru_cache_del(range, GetMemMapIndex(pageaddr))

#define lru_add_by_range_type(range_type, pageaddr) \
		lru_add(range_gotten_by_type(range_type), pageaddr);

#define lru_del_by_range_type(range_type, pageaddr) \
		lru_del(range_gotten_by_type(range_type), pageaddr);

/**
 * 先关中断，再把页描述符加入到LRU
 */
static inline void lru_add_by_range_type_irqsave
(unsigned long range_type, unsigned long pageaddr)
{	
	unsigned long flags;
	interrupt_save_and_disable(flags);	
	lru_add_by_range_type(range_type, pageaddr);	
	interrupt_restore_state(flags);
}

/**
 * 先关中断，再把页描述符加入到LRU
 */
static inline void lru_add_irqsave
(mem_range_t* range, struct page* page) 
{
	unsigned long flags;
	interrupt_save_and_disable(flags);	
	lru_cache_add(range, page);	
	interrupt_restore_state(flags);
}

#endif