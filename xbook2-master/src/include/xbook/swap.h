#ifndef _XBOOK_SWAP_H
#define _XBOOK_SWAP_H

#include <xbook/mutexlock.h>
#include <xbook/rb_tree.h>
#include <xbook/list.h>
#include <xbook/task.h>
#include <arch/mempool.h>
#include <types.h>

/* 交换项 */
typedef unsigned int swap_entry_t;

struct swap_entry_cluster {
    unsigned int type;
    unsigned int index;
    unsigned long count;
};

#define SWP_INDEX(entry) (entry >> 8)
#define SWP_TYPE(entry)  ((entry & 0x7f) >> 1)
#define SWP_ENTRY(i, type) ((i << 8) | (PAGE_ATTR_PROTNONE) | (type << 1))



/* swap_header填充物大小 */
#define FILLING_SIZE 4078
#define MAGIC_SIZE 10

extern const char* magic;

/**
 * 交换区头部结构体
 */
struct swap_header {
    /* 交换区的块的数量（块=4096B） */
    unsigned long nr_blocks;
    /* 交换区的大小（单位：字节） */
    unsigned long space_size;
    /* 填充物 */
    char filling[FILLING_SIZE];
    /* 魔数“SWAP_SPACE”，用于检测 */
    char magic[MAGIC_SIZE];
}__attribute__ ((packed));

/**
 * 交换区子区的结构体
 */
struct seed_swap_struct {
    /* 在空间内的起始索引 */
    unsigned long index_space;
    /* 在交换区内的起始块 */
    unsigned long start_block;
    /* 连续块的数量 */
    unsigned long nr_blocks;
    int revmap_id;
    /* 链接待换出物理页的描述符 */
    list_t page_head;
    /* 交换区sendswap_root的子节点 */
    rbtree_node rb_node;
    /* 链接range的待换出链表 */
    list_t sendswap_node;
};

#define GetSeedSwpFromNode(entry) container_of(entry, struct seed_swap_struct, sendswap_node)
#define GetSeedSwpFromRbn(entry)  container_of(entry, struct seed_swap_struct, rb_node)

/* 交换槽最大的可用计数值 */
#define SWAP_MAP_MAX  0x7F
/* 不可用的槽 */
#define SWAP_MAP_BAD  0x80
/* 连续的交换槽的最大数 */
#define SWAPFILE_CLUSTER 256

/* 表示交换区已经被使用 */
#define SWAP_USED    0x1
/* 交换区可以被写入数据 */
#define SWAP_WRITE   0x2
/* 交换区正在被写入数据 */
#define SWAP_WRITING 0x4

/* 指定交换区优先级 */
#define SWAP_FLAG_PRIO 0x8000
/* 交换区的最大优先级 */
#define SWAP_PRIO_MASK 0x7FFF

#define MAX_SWAPFILE 16

/** 
 * 记录交换区正在被使用的状态的描述符
 */
struct swap_info_struct {
    unsigned int flags;
    /* 保护交换区的自旋锁 */
	mutexlock_t sdev_lock;
    /* 记录交换区子区的根root */
    rbtree_root sendswap_root;
    /* 文件/块设备的fd */
	int fd;
    /* 记录当前交换区内的块的使用情况 */
	unsigned char* swap_map;
     /* 最近的空闲块的位置 */
	unsigned int lowest_bit;
    /* 最后一个空闲块的位置 */
	unsigned int highest_bit;
    /* 在交换区内连续的块的起始位置 */
	unsigned int cluster_next;
    /* 在交换区内连续的块的剩余总数 */
	unsigned int cluster_nr;
    /* 记录空闲的块总数 */
    unsigned int nr_free_blocks;
	/* 记录可用块的总数 */
	unsigned int nr_blocks;
    /* 交换区优先级 */
    int prio;
    /* 指向下一个比当前交换区优先级小或相同的交换区的索引 */
    int next;
};

extern char *swapfile_path;
extern struct swap_info_struct swap_info[MAX_SWAPFILE];
extern mutexlock_t swapinfo_lock;
extern unsigned int used_swap_info;
extern int page_cluster;
extern task_t* kswapd_thread;

/**
 * 交换区优先级链表
 */
struct swap_list_t {
    /* 记录优先级最高的交换区的索引 */
    int head; 
    /* 记录下一个将被用到的交换区索引 */
    int next;
};

extern struct swap_list_t swap_list;
extern mutexlock_t swaplist_lock;

void range_try_to_swap_pages(mem_range_t* range);
void write_swap_page();
void do_swap_fault(unsigned long vaddr, swap_entry_t swap_entry, mem_space_t* space);
void swap_init();
void kswapd_init();
int alloc_page_to_read(struct seed_swap_struct* seed_swp, unsigned long type);
void activate_swappage(unsigned int type, struct seed_swap_struct* seed_swp, mem_space_t* space, int read);

int sys_mkswap(const char* filepath);
int sys_swapon(const char* swapfile, int flags);
int sys_swapoff(const char* swapfile);
void get_swap_block(unsigned int count, struct swap_entry_cluster* array);
void swap_free(struct seed_swap_struct* seed_swp, struct swap_info_struct* p);

static inline void swap_list_lock() {
    mutex_lock(&swaplist_lock);
}
static inline void swap_list_unlock() {
    mutex_unlock(&swaplist_lock);
}

static inline void 
swap_sdev_lock(struct swap_info_struct* swap_info) {
    mutex_lock(&swap_info->sdev_lock);
}
static inline void 
swap_sdev_unlock(struct swap_info_struct* swap_info) {
    mutex_unlock(&swap_info->sdev_lock);
}

#endif