#ifndef _XBOOK_SWAP_STAT_H
#define _XBOOK_SWAP_STAT_H

#include <xbook/mutexlock.h>
#include <xbook/radix_tree.h>
#include <xbook/rb_tree.h>
#include <xbook/mm.h>
#include <xbook/swap.h>

/**
 * 交换高速缓存
 */
struct swapper_space
{
    /* 保护该结构体的互斥锁 */
    mutexlock_t list_lock;
    /* 记录页的数量 */
    unsigned long nr_pages;
    /* 把pfn作为key，记录页的基数树 */
    radix_tree_t pfn_rtree;
    /* 把index和type交换项作为key，记录页的基数树 */
    radix_tree_t index_rtree;
};

extern struct swapper_space swapper_space;

void add_to_page_cache(struct page* page, unsigned int index);
int add_page_to_cache_unique(struct page* page, unsigned long index);
void remove_page_from_page_cache(struct page* page);
int remove_page_from_cache_unique(struct page* page);
struct page* find_page_by_pfn(struct page* page);
struct page* find_page_by_index(unsigned long index);

int swap_duplicate(swap_entry_t entry);
int add_to_swap_cache(struct page* page, swap_entry_t entry);
void swap_setup(void);
rbtree_node* find_node_from_swapinfo(struct swap_info_struct* p, unsigned long value);

#endif