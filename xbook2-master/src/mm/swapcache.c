#include <xbook/swap.h>
#include <xbook/swap_stat.h>
#include <xbook/debug.h>


/**
 * 通过页描述符在mem_map的索引，即pfn，把页描述符的地址记录
 * 到基数树内
 */
static inline int add_page_to_pfn_rtree
(struct page* page, struct swapper_space* mapping)
{
    return radix_tree_insert(&mapping->pfn_rtree, GetPfn(page), page);
}

/**
 * 通过index，把页描述符的地址记录到基数树内
 */
static inline int add_page_to_index_rtree
(struct page* page, struct swapper_space* mapping)
{
    return radix_tree_insert(&mapping->index_rtree, page->swp_entry, page);
}

/**
 * 把页描述符的地址从pfn基数树删除
 */
static inline void del_page_from_pfn_rtree
(struct page* page, struct swapper_space* mapping)
{
    radix_tree_delete(&mapping->pfn_rtree, GetPfn(page));
}

/**
 * 把页描述符的地址从index基数树删除
 */
static inline void del_page_from_index_rtree
(struct page* page, struct swapper_space* mapping)
{
    radix_tree_delete(&mapping->index_rtree, page->swp_entry);
}

/**
 * 通过pfn作为key，在交换高速缓存的基数树搜索页，如果有返回
 * 页描述符地址，否则返回NULL
 */
static inline struct page* find_page_in_pfn_rtree_nolock
(struct page* page, struct swapper_space* mapping)
{
    return radix_tree_find(&mapping->pfn_rtree, (u32_t)page);
}

/**
 * 通过index作为key，在交换高速缓存的基数树搜索页，如果有
 * 返回页描述符地址，否则返回NULL
 */
static inline struct page* find_page_in_index_rtree_nolock
(unsigned long index, struct swapper_space* mapping)
{
    return radix_tree_find(&mapping->index_rtree, index);
}


/**
 * 把页加入到高速缓存的本体，需上锁再执行该函数
 */
static inline void __add_to_page_cache(struct page* page, unsigned long index)
{
    page->flags &= ~(1 << PG_referenced | 
            1 << PG_uptodate | 1 << PG_dirty | 1 << PG_launder);

    page->swp_entry = index;
    page->mapping = (void*)&swapper_space;
    if (add_page_to_pfn_rtree(page, &swapper_space))
        panic("swapper_space: failed to add page to pfn rtree.\n");
    if (add_page_to_index_rtree(page, &swapper_space))
        panic("swapper_space: failed to add page to index rtree.\n");
    
    swapper_space.nr_pages++;
}

/**
 * 把页加入到高速缓存的接口函数
 */
void add_to_page_cache(struct page* page, unsigned int index)
{
    mutex_lock(&swapper_space.list_lock);
    __add_to_page_cache(page, index);
    mutex_unlock(&swapper_space.list_lock);
}

/**
 * 先做检查，再把页加入到高速缓存，成功返回0，否则返回-1
 */
int add_page_to_cache_unique(struct page* page, unsigned long index)
{
    int err = 0;
    struct page* check_page = NULL;

    mutex_lock(&swapper_space.list_lock);
    check_page = find_page_in_pfn_rtree_nolock(page, &swapper_space);
    if (check_page) 
        err = -1;
    else
        __add_to_page_cache(page, index);

    mutex_unlock(&swapper_space.list_lock);
    return err;
}

/**
 * 把页从高速缓存删除的本体，需上锁再执行该函数
 */
static inline void __remove_page_from_page_cache(struct page* page)
{
    del_page_from_pfn_rtree(page, &swapper_space);
    del_page_from_index_rtree(page, &swapper_space);
    swapper_space.nr_pages--;
    page->mapping = NULL;
    page->swp_entry = 0;
}

/**
 * 把页从高速缓存删除的接口函数
 */
void remove_page_from_page_cache(struct page* page)
{
    mutex_lock(&swapper_space.list_lock);
    __remove_page_from_page_cache(page);
    mutex_unlock(&swapper_space.list_lock);
}

/**
 * 先做检查，再把页从高速缓存删除，成功返回0，否则返回-1
 */
int remove_page_from_cache_unique(struct page* page)
{  
    int ret_val = 0;

    mutex_lock(&swapper_space.list_lock);
    if (!find_page_in_pfn_rtree_nolock(page, &swapper_space))
        ret_val = -1;
    else 
        __remove_page_from_page_cache(page);

    mutex_unlock(&swapper_space.list_lock);
    return ret_val;
}

/**
 * 使用pfn在swapper_space的pfn基数树查找页描述符
 */
struct page* find_page_by_pfn(struct page* page)
{
    mutex_lock(&swapper_space.list_lock);
    struct page* status = find_page_in_pfn_rtree_nolock(page, &swapper_space);
    mutex_unlock(&swapper_space.list_lock);
    return status;
}

/**
 * 使用index在swapper_space的pfn基数树查找页描述符
 */
struct page* find_page_by_index(unsigned long index)
{
    mutex_lock(&swapper_space.list_lock);
    struct page* status = find_page_in_index_rtree_nolock(index, &swapper_space);
    mutex_unlock(&swapper_space.list_lock);
    return status;
}