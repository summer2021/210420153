#include <xbook/radix_tree.h>
#include <xbook/list.h>
#include <xbook/memcache.h>
#include <xbook/debug.h>
#include <string.h>
#include <stddef.h>

/* 默认基树的内存池的大小 */
#define POOL_SIZE   (32 * PAGE_SIZE)
/* KRY值的位数（单位：bit） */ 
#define KEY_BITS    2
/* 32位地址 */
#define ADDR32_BITS 32
/* 基树节点的child数组的数量 */
#define NR_CHILD    4
#define MAGIC_SIZE  10

/**
 * 空闲节点的默认数量，如果rtree->free为空，则从内存池分配
 * 512个空闲节点 
 */
#define DEF_FNODE_COUNT 512

/**
 * 如果rtree->nr_fnodes >= highest_fnode，则表示需要释放
 * free链表内的空闲节点，直到nr_fnodes <= highest_fnode
 */
#define HIGHEST_FNODE 1024

#define AddrToIndex(addr) ((unsigned int)addr >> PAGE_SHIFT)
#define IndexToAddr(index) ((unsigned int)index << PAGE_SHIFT)

#define RIGHT_MOVE(key) (key >> KEY_BITS) 
#define CHECK_BITS(key, pos) \
        (((unsigned int)key << (ADDR32_BITS - (pos+1)*KEY_BITS)) \
        >> (ADDR32_BITS - KEY_BITS))

static char* const rpool_magic = "Radix-Pool";

/**
 * 基树节点结构体
 */
typedef struct radix_node {
    union {
        /** 
         *当节点空闲时，加入到radix_tree的空闲节点链表当中 
         */
        struct list next_fnode;
        /** 
         * 记录下一个基数节点（子节点），根据KEY值找出相应的子节点 
         */
        struct radix_node* child[NR_CHILD];    
    };
    /* 记录上一个基树节点（父节点） */
    struct radix_node* parent;
    /* 记录字段：child数组内的已使用元素的计数 */
    u8_t nr_used_cnodes;
    /* 存储page结构体的地址 */
    struct page* page;
}__attribute__ ((packed))radix_node_t;

/**
 * 基树的内存池
 * 作用：当分配/释放节点时，减少内存分配/释放时间。以页为单位进行
 *      分配给内存池，页的开头存储内存池的结构体，剩下的内存用于
 *      节点的分配。
 */
typedef struct radix_pool  {
    list_t rpool_lnode;
    /* radix_pool+1 */
    radix_node_t* start;
    /* 记录下一个空闲的节点索引 */
    int next_free_node;
    /* 记录最前面的空闲节点索引 */
    int lowest_free_node;
    /* 记录最后面的空闲节点索引 */
    int highest_free_node;
    /* 记录当前内存池的节点总数量 */
    unsigned int nr_nodes;
    /* 记录空闲节点的数量 */
    unsigned int nr_free_nodes;
    char magic[MAGIC_SIZE];
}radix_pool;

static inline void clear_child_node(radix_node_t* node)
{
    for (int i = 0; i < 4; i++)
        node->child[i] = NULL;
}

/**
 * 分配一个新的内存池
 */
static radix_pool* alloc_new_pool(radix_tree_t* rtree)
{
    radix_pool* new_pool = (radix_pool*)mem_alloc(POOL_SIZE);
    if (!new_pool)
        return NULL;

    list_init(&new_pool->rpool_lnode);
    new_pool->start = (radix_node_t*)(new_pool + 1);
    new_pool->nr_free_nodes = \
    new_pool->nr_nodes = (POOL_SIZE - sizeof(radix_pool)) / 
                         sizeof(radix_node_t); 
    new_pool->lowest_free_node = \
    new_pool->next_free_node = 0;
    new_pool->highest_free_node = new_pool->nr_nodes - 1;

    mutex_lock(&rtree->rpool_lock);
    if (rtree->pool)
        list_add(&new_pool->rpool_lnode, &rtree->pool->rpool_lnode);
    else 
        rtree->pool = new_pool;
    mutex_unlock(&rtree->rpool_lock);
    
    strcpy(new_pool->magic, rpool_magic);

    return new_pool;
}

/**
 * 释放内存池，前提是内存池内的节点总数量=空闲节点的数量
 */
static int free_pool_nolock(radix_tree_t* rtree, radix_pool* rpool)
{
    if (rpool->nr_nodes != rpool->nr_free_nodes)
        return -1;
    
    list_del(&rpool->rpool_lnode);
    rpool->lowest_free_node = \
    rpool->next_free_node = \
    rpool->nr_free_nodes = \
    rpool->nr_nodes = 0;
    rpool->start = NULL;
    memset((void*)rpool->magic, 0, MAGIC_SIZE);

    mem_free(rpool);
    return 0;
}

/**
 * 创建一个新节点，优先从rtree->free获取
 */
static radix_node_t* radix_node_alloc(radix_tree_t* rtree)
{
    radix_node_t *node, *start;
    radix_pool *rpool;
    list_t *list_node, *list_head;
    unsigned int fnodes_count, i, index;
    unsigned int next_free_node, lowest_free_node, 
                 highest_free_node, highest_next_node;

    mutex_lock(&rtree->rpool_lock);
    /**
     * 如果没有空闲的节点，则从内存池获取，如果没有空闲的内存池，
     * 则请求分配内存给内存池，再获取空闲节点。如果请求内存失败，
     * 则返回NULL。 
     */
    if (!rtree->nr_fnodes) {
        node = NULL;
        fnodes_count = index = 0;
find_free_pool:
        list_node = list_head = &(rtree->pool->rpool_lnode);
        do {
            rpool = container_of(list_node, radix_pool, rpool_lnode);
            if (rpool->nr_free_nodes) 
                goto get_free_node;

            list_node = list_node->next;
        } while (list_node != list_head);

        if (list_node == list_head) {
            if (!(rpool = alloc_new_pool(rtree))) {
                if (!list_empty(&rtree->free))
                    goto get_new_node;
                else
                    goto ret;
            }
        }
                
get_free_node:
        fnodes_count = DEF_FNODE_COUNT - rtree->nr_fnodes;
        if (rpool->nr_free_nodes < fnodes_count)
            fnodes_count = rpool->nr_free_nodes;
        
        next_free_node = rpool->next_free_node;
        lowest_free_node = rpool->lowest_free_node;
        highest_free_node = rpool->highest_free_node;
        highest_next_node = highest_free_node + 1;
        start = rpool->start;
        for (i = 0; i < fnodes_count; i++) {
            /*if (next_free_node >= 0)*/ {
                index = next_free_node++;
                list_add(&start[index].next_fnode, &rtree->free);
                rtree->nr_fnodes++;
                rpool->nr_free_nodes--;

                for (; next_free_node < highest_next_node &&  \
                     (start[next_free_node].nr_used_cnodes || \
                     start[next_free_node].page); next_free_node++);
                    
                if (lowest_free_node == index)
                    lowest_free_node = next_free_node;

                /* 最坏的情况 */
                if (next_free_node >= highest_next_node) {
                    if (lowest_free_node < highest_next_node) {
                        next_free_node = lowest_free_node;
                        for (; highest_free_node >= lowest_free_node &&  \
                             (start[highest_free_node].nr_used_cnodes || \
                             start[highest_free_node].page); highest_free_node--);
                    } else {
                        lowest_free_node = rpool->nr_nodes;
                        highest_free_node = next_free_node = -1;
                    }  
                }
            }

            /* 旧版本，以防万一，还是暂时留下吧
            if (next_free_node >= nr_nodes) 
                next_free_node = lowest_free_node;

            index = next_free_node++;

            for (; ((start[next_free_node].nr_used_cnodes ||   \
                 start[next_free_node].page) && next_free_node \
                   < nr_nodes); next_free_node++);

            if (lowest_free_node == index)
                lowest_free_node = next_free_node;

            list_add(&start[index].next_fnode, &rtree->free);
            rtree->nr_fnodes++;
            rpool->nr_free_nodes--;*/
        }
        rpool->next_free_node = next_free_node;
        rpool->lowest_free_node = lowest_free_node;
        rpool->highest_free_node = highest_free_node;

        if (rtree->nr_fnodes < DEF_FNODE_COUNT) 
            goto find_free_pool;
    }

get_new_node:
    node = container_of(rtree->free.next, radix_node_t, next_fnode); 
    list_del(rtree->free.next);
    rtree->nr_fnodes--;
    clear_child_node(node);
	node->parent = NULL;
	node->page = NULL;
    node->nr_used_cnodes = 0;
ret:
    mutex_unlock(&rtree->rpool_lock);
    return node;
}

/**
 * 尝试释放多余的空闲节点，如果内存池内的节点总数量=空闲节点的数量，
 * 则顺便释放内存池
 */
static void try_to_shrink_free_nodes(radix_tree_t* rtree)
{
    unsigned int free_nodes, addr, index;
    radix_node_t *fnode;
    radix_pool* rpool;
    
    /**
     * 从尾部释放，因为后面的空闲节点没有相应的CPU缓存行。
     * 前面的空闲节点还存在着相应的缓存行，大概率会被复用。
     */
    mutex_lock(&rtree->rpool_lock);
    if (rtree->nr_fnodes > HIGHEST_FNODE) {    
        free_nodes = rtree->nr_fnodes - HIGHEST_FNODE;
        rtree->nr_fnodes -= free_nodes;

        while (free_nodes--) {    
            fnode = container_of(rtree->free.prev, radix_node_t, next_fnode);
            addr = (unsigned int)fnode;
            index = AddrToIndex(addr);
        loop:
            rpool = (radix_pool*)IndexToAddr(index--);
            if (strcmp(rpool->magic, rpool_magic) != 0)
                goto loop;

            if (rpool->lowest_free_node > (int)(fnode - rpool->start))
                rpool->lowest_free_node = (int)(fnode - rpool->start);
            
            if (rpool->highest_free_node < (int)(fnode - rpool->start))
                rpool->highest_free_node = (int)(fnode - rpool->start);

            if (rpool->next_free_node < 0)
                rpool->next_free_node = rpool->lowest_free_node;

            if (rpool->nr_free_nodes == rpool->nr_nodes)
                free_pool_nolock(rtree, rpool);  

            list_del(&fnode->next_fnode);
            rpool->nr_free_nodes++;
        }
    }
    mutex_unlock(&rtree->rpool_lock);
}

/** 
 * 在rtree插入一个新节点 
 */
int radix_tree_insert(radix_tree_t* rtree, u32_t key, struct page* page)
{
    int index;
    radix_node_t *node, *child;
    node = rtree->root;

    mutex_lock(&rtree->rtree_lock);
    do {
        index = CHECK_BITS(key, 0);
        if (!node->child[index]) {
            if (!(child = radix_node_alloc(rtree)))
                return -1;
            
            child->parent = node;
            node->child[index] = child;
            node->nr_used_cnodes++;
        }

        node = node->child[index];
        key = RIGHT_MOVE(key);
    } while (key);

    if (node->page == page)
        return INSERT_VALUE_EXISTED;
    
    node->page = page;
    mutex_unlock(&rtree->rtree_lock);
    return 0;
}

/**
 * 根据key从下往上删除基树内的节点 
 */
int radix_tree_delete(radix_tree_t* rtree, u32_t key)
{
    radix_node_t *node, *par;
    int index, floors_count, status;
    u32_t cur_key;

    status = 0;
    mutex_lock(&rtree->rtree_lock);
    if (!(node = rtree->root)) {
        status = RADIX_DELETE_ERROR;
        goto _ret;
    }

    cur_key = key;
    floors_count = 0;

    /**
     * 先从上往下开始遍历检查
     */
    do {
        index = CHECK_BITS(cur_key, 0);
        node = node->child[index];
        cur_key = RIGHT_MOVE(cur_key);
        floors_count++;
    } while (cur_key && node);

    if (!node) 
        goto _ret;

    /** 
     * 表示下面还存在着子节点，所以仅删除page，然后返回 
     */
    if (node->nr_used_cnodes) {
        node->page = NULL;
        goto _ret;
    }

    /**
     * 现在开始从下往上释放基树内的节点
     */
    while (floors_count--) {
        par = node->parent;
        par->child[index] = NULL;
        par->nr_used_cnodes--;
        clear_child_node(node);
        node->page = NULL;
        list_add(&node->next_fnode, &rtree->free);
        rtree->nr_fnodes++;

        if (!par->parent || par->page || par->nr_used_cnodes)
            goto ret;

        index = CHECK_BITS(key, floors_count-1);
        node = par;
    }

ret:
    try_to_shrink_free_nodes(rtree);
_ret:
    mutex_unlock(&rtree->rtree_lock);
    return status;
}

/**
 * 搜索节点
 * 以key为索引，找到了并且有page，则返回page，否则返回NULL
 */
struct page* radix_tree_find(radix_tree_t *rtree, u32_t key)
{
    radix_node_t* node;
    int index;
    struct page* page = NULL;

    mutex_lock(&rtree->rtree_lock);
    if (!(node = rtree->root))
        goto ret;

    do {
        index = CHECK_BITS(key, 0);
        node = node->child[index];
        key = RIGHT_MOVE(key);
    } while (key && node);
    
    if (node)
        page = node->page;

ret:
    mutex_unlock(&rtree->rtree_lock);
    return page;
}

/**
 * 初始化基树radix_tree
 */
void radix_tree_init(radix_tree_t* rtree)
{
    mutexlock_init(&rtree->rtree_lock);
    mutexlock_init(&rtree->rpool_lock);
    list_init(&rtree->free);
    rtree->pool = NULL;
    rtree->nr_fnodes = 0;
    
    rtree->pool = alloc_new_pool(rtree);
    rtree->root = radix_node_alloc(rtree);

#ifdef DEBUG_RTREE
    keprint(PRINT_INFO "radix tree: init done.\n\
    the used nodes in the rtree: 1\n\
    the free nodes in the rtree: %d\n\
    the nodes in the pool: %d\n\
    the free nodes in the pool: %d\n", 
    rtree->nr_fnodes, 
    rtree->pool->nr_nodes,
    rtree->pool->nr_free_nodes);
#endif
}