#include <sys/syscall.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * 错误码
 */
enum err_code {
    /* the file has been opened by the other tasks */
    EOPEN1 = 10,
    /* the file does not exist */
    EOPEN2,
    /* the status of the file cannot be gotten. */
    ESTAT,
    /* free memory cannot be gotten */
    EGMEM,
    /* the data in the file cannot be written in the memory from the disk */
    EREAD,
    /* the size of the file is smaller than 20MiB */
    ESIZE,
    /* the magic in the swap_header == swap_header_magic */
    ESAME,
    /* the file pointer cannot be set */
    ELSEEK,
    /* the data in the memory cannot be written in the disk */
    EWRITE,
};

/**
 * 错误码对应的字符串
 */
static const char* erropen1_str = "failed to creat swap space as the file has been opened by the other tasks.";
static const char* erropen2_str = "failed to open this file, please examine the file path.";
static const char* errstat_str = "failed to get the status of the file";
static const char* errgmem_str = "failed to get memory. perhaps there is no free memory.";
static const char* errread_str = "failed to get swap_header from the file in the disk.";
static const char* errsize_str = "the size of the file is smaller than 20MiB. \n\
        please choose the other file of up to 20MiB or more than 20MiB to create a swap space.";
static const char* errsame_str = "the swap space has been made from the file.";
static const char* errlseek_str = "failed to set the address in the file to write swap_header in it.";
static const char* errwrite_str = "failed to write the swap_header in the file.";

int main(int argc, char *argv[])
{
    int ret_value;
    const char* str_array[] = {erropen1_str, erropen2_str, errstat_str, 
                errgmem_str, errread_str, errsize_str, errsame_str, 
                errlseek_str, errwrite_str};

    if (argc == 1) {
        fprintf(stderr, "mkswap: arguments is too less.\n");
        return -1;
    }

    if ((ret_value = syscall1(int, SYS_MKSWAP, argv[1])) >= 0)
        fprintf(stdout, "mkswap: finish creating the swap space!\n\
        the size of the swap space is %dMiB (%d).\n", ret_value/1024/1024, ret_value);
    else 
        fprintf(stdout, "mkswap: %s\n", str_array[abs(ret_value + 10)]);
    return 0;
}