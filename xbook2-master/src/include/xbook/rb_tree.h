#ifndef _XBOOK_RB_TREE_H
#define _XBOOK_RB_TREE_H

#include <stddef.h>

/**
 * 红黑树节点的颜色
 */
enum rbtree_node_color
{
    RB_RED = 0,
    RB_BLACK
};

/**
 * 红黑树节点结构体
 */
typedef struct rb_tree_node
{
    /* 记录父节点 */
    struct rb_tree_node *parent;
    /* 记录左右节点 */
    struct rb_tree_node *left, *right;
    /* 单个节点的颜色 */
    unsigned int color;
    /* 搜索时使用的关键值 KRY */
    int value;
}rbtree_node;

/**
 * 红黑树根节点结构体
 */
typedef struct rb_tree_root
{
    rbtree_node* root;
    /* 叶子节点，统称：NIL */
    rbtree_node NIL;
}rbtree_root;

rbtree_node* get_smallest_node(rbtree_node* node, rbtree_root* rbtree);
void rbtree_insert(rbtree_node* new_node, rbtree_root* rbtree);
void delete_case(rbtree_node* node, rbtree_root* rbtree);
rbtree_node* rbtree_delete(rbtree_node* node, rbtree_root* rbtree);
rbtree_node* find_node(rbtree_root* rbtree, int value);
//void rbtree_test();

/**
 * 初始化节点
 */
static inline void 
init_rbtree_node(rbtree_node* node, int value, rbtree_root* rbt_root)
{
    node->parent = NULL;
    node->left = node->right = &rbt_root->NIL;
    node->color = RB_RED;
    node->value = value;
}

static inline void init_rbtree(rbtree_root* rbt_root)
{
    rbt_root->root = NULL;
    rbt_root->NIL.color = RB_BLACK;
    rbt_root->NIL.value = 0;
    rbt_root->NIL.left = \
    rbt_root->NIL.right = \
    rbt_root->NIL.parent = NULL;
}

#endif