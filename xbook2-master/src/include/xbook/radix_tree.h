#ifndef _XBOOK_RADIX_TREE_H
#define _XBOOK_RADIX_TREE_H

#include <xbook/mutexlock.h>
#include <xbook/mm.h>
#include <types.h>

/**
 * 基树结构体
 */
typedef struct radix_tree {
    /* 保护基树的锁 */
    mutexlock_t rtree_lock;
    /* 根节点 */
    struct radix_node* root;
    /** 
     * 填充物，目的是防止一个CPU的L1缓存行中存在多个自旋锁，
     * 如果缓存行内存在两个及以上的锁，且被两个CPU使用不同的
     * 锁会影响多核缓存一致性
     * 
     * 32为CPU的L1缓存行（指令）的大小，这个大小可能不准确，
     * 可能XBOOK2的作者没写关于CPU的cache的代码，也可能是
     * 我没发现
     */
    // char filling[32 - sizeof(mutexlock_t) - sizeof(struct radix_node*)];
    /* 保护下面的字段的锁 */
    mutexlock_t rpool_lock;
    /* 基树的内存池 */
    struct radix_pool* pool;
    /* 记录不在基树内的空闲节点 */
    struct list free;
    /* 记录空闲节点的总数 */
    unsigned long nr_fnodes;
}radix_tree_t;

/* 插入节点时发现值已存在 */
#define INSERT_VALUE_EXISTED -2
/* 删除节点时发现root节点不存在 */
#define RADIX_DELETE_ERROR   -2

void radix_tree_init(radix_tree_t* rtree);
int radix_tree_insert(radix_tree_t* rtree, u32_t key, struct page* page);
int radix_tree_delete(radix_tree_t* rtree, u32_t key);
struct page* radix_tree_find(radix_tree_t *rtree, u32_t key);

#endif