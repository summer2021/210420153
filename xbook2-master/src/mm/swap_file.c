#include <xbook/swap.h>
#include <xbook/swap_stat.h>
#include <xbook/debug.h>
#include <xbook/memcache.h>
#include <xbook/fs.h>
#include <xbook/rb_tree.h>
#include <xbook/mm.h>
#include <xbook/memspace.h>
#include <xbook/lru.h>
#include <arch/mempool.h>
#include <arch/page.h>
#include <string.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG_MKSWAP
#define DEBUG_SWAP

/**
 * 通用错误码
 */
enum errcode {
    /* the file has been opened by the other tasks */
    EOPEN1 = 10,
    /* the file does not exist */
    EOPEN2,
    /* the status of the file cannot be gotten. */
    ESTAT,
    /* free memory cannot be gotten */
    EGMEM,
    /* the data in the file cannot be written in the memory from the disk */
    EREAD,
};

/**
 * mkswap错误码
 */
enum mkswap_errcode {
    /* the size of the file is smaller than 20MiB */
    ESIZE = 15,
    /* the magic in the swap_header == swap_header_magic */
    ESAME,
    /* the file pointer cannot be set */
    ELSEEK,
    /* the data in the memory cannot be written in the disk */
    EWRITE,
};

/**
 * swapon错误码
 */
enum swapon_errcode {
    /* there is no swap_header in the header of the file */
    ENOSWP = 15,
    /* the size of the file is different from before */
    ESZDIF,
    /* there is no free member in the swap_info array */
    ENINFO,
};

/** 
 * swapoff错误码
 */
enum swapoff_errcode {
    /* the file "/swapfile" isn't closed */
    EDFSWP = 15,
    /* the file hasn't been opened */
    ENOPEN,
    /* the file is not swap space */
    ENSWPS,
    /* the swap space is being written */
    EWRITING,
    /* one of seed_swaps in swap_info is in swapout_list */
    EINSWPL,
};

/* 交换区的大小标准为20MB */
#define SWAPSPACE_DEF_SIZE 0x1400000

const char* swap_header_magic = "SWAP-SPACE";

unsigned long nr_swap_blocks = 0;
unsigned long total_swap_blocks = 0;

struct swap_list_t swap_list = {-1, -1};
mutexlock_t swaplist_lock;

static list_t swapout_list;

/**
 * 创建交换区
 */
int sys_mkswap(const char* filepath)
{
    int fd, result = 0;
    struct stat swapfile_stat;
    struct swap_header* swap_header;

    if (kfile_filecmp(filepath, &fd) == 1) {
		if (fd != -1) {
			result = -EOPEN1;
        	goto ret;
		}
	}
    if ((fd = kfile_open(filepath, O_RDWR)) < 0) {
        result = -EOPEN2;
        goto ret;
    }
    if (kfile_stat(filepath, &swapfile_stat) < 0) {
        result = -ESTAT;
        goto close_file;
    }
    if (swapfile_stat.st_size < SWAPSPACE_DEF_SIZE) {
        result = -ESIZE;
        goto close_file;
    }
    if (!(swap_header = (struct swap_header*)mem_alloc(sizeof(struct swap_header)))) {
        result = -EGMEM;
        goto close_file;
    }

    if (kfile_read(fd, (void*)swap_header, sizeof(struct swap_header)) < 0) {
        result = -EREAD;
        goto free_mem;
    } else if (strcmp(swap_header->magic, swap_header_magic) == 0) {
        if (swapfile_stat.st_size == swap_header->space_size) {
            result = -ESAME;
            goto free_mem;
        } else {
            keprint(PRINT_INFO "mkswap: the size of this swap space is smaller or bigger then it was before.\n\
        so, the swap space will be created again.\n");
        }
    }

    swap_header->space_size = swapfile_stat.st_size;
    swap_header->nr_blocks = swapfile_stat.st_size / PAGE_SIZE;
    strcpy(swap_header->magic, swap_header_magic);
    if (kfile_lseek(fd, 0, SEEK_SET) < 0) {
        result = -ELSEEK;
        goto free_mem;
    }
    if (kfile_write(fd, (void*)swap_header, PAGE_SIZE) < 0) {
        result = -EWRITE;
        goto free_mem;
    }
    keprint(PRINT_INFO "mkswap: finish creating the swap space!\n\
        the size of the swap space is %dMiB (%d).\n", 
        swap_header->space_size/1024/1024, swap_header->space_size);
    
    result = swap_header->space_size;
    
#ifdef DEBUG_MKSWAP
    struct swap_header* swap_header2;
    swap_header2 = (struct swap_header*)mem_alloc(sizeof(struct swap_header));
    kfile_lseek(fd, 0, SEEK_SET);
    kfile_read(fd, (void*)swap_header2, sizeof(struct swap_header));
    keprint(PRINT_DEBUG "mkswap debug: \n\
        swap_header->nr_blocks:%d,\n\
        swap_header->space_size:%d, \n\
        swap_header->magic:%s\n", 
        swap_header2->nr_blocks, swap_header2->space_size, 
        swap_header2->magic);
    mem_free(swap_header2);
#endif

free_mem:   
    mem_free(swap_header);
close_file:
    kfile_close(fd);
ret:
    return result;
}

/**
 * 打开交换区 
 */
int sys_swapon(const char* swapfile, int flags)
{
    int i, fd, maxblocks, prev = -1, result = 0;
    static int least_priority = 0;
    struct stat swapfile_stat;
    struct swap_info_struct *cur;
    struct swap_header* swap_header;

    if (kfile_filecmp(swapfile, &fd)) {
		if (fd != -1) {
			result = -EOPEN1;
       		goto ret;
		}
	}
    if ((fd = kfile_open(swapfile, O_RDWR)) < 0) {
        result = -EOPEN2;
        goto ret;
    }
    if (kfile_stat(swapfile, &swapfile_stat) < 0) {
        result = -ESTAT;
        goto close_file;
    }
    if (!(swap_header = (struct swap_header*)mem_alloc(sizeof(struct swap_header)))) {
        result = -EGMEM;
        goto close_file;
    }

    if (kfile_read(fd, (void*)swap_header, sizeof(struct swap_header)) < 0) {
        result = -EREAD;
        goto free_mem;
    } else if (!strcmp(swap_header->magic, swap_header_magic)) {
        result = -ENOSWP;
        goto free_mem;
    } else if (swapfile_stat.st_size != swap_header->space_size) {
        result = -ESZDIF;
        goto free_mem;
    }

    cur = swap_info;
    for (i = 0; i < MAX_SWAPFILE; i++, cur++) {
        if (!(cur->flags & SWAP_USED))
            break;
    }
    if (i >= MAX_SWAPFILE) {
        result = -ENINFO;
        goto free_mem;
    }

    maxblocks = SWP_INDEX(SWP_ENTRY(~0, 0));
    if (swap_header->nr_blocks < maxblocks)
        maxblocks = swap_header->nr_blocks;

    /* 在NORAML区域分配内存给swap_map */
    if (!(cur->swap_map = (unsigned char*)mem_alloc(maxblocks * sizeof(char)))) {
        result = -EGMEM;
        keprint(PRINT_ERR "swapon: failed to get free memory to make swap_map.\n");
        goto free_mem;
    }
    memset(cur->swap_map, 0, maxblocks * sizeof(char));
    cur->flags = SWAP_USED; /* 设置交换区的状态，先设置可用（可读）的状态 */
    mutexlock_init(&cur->sdev_lock);    /* 初始化互斥锁锁 */
    init_rbtree(&cur->sendswap_root);   /* 初始化红黑树 */
    cur->fd = fd;           /* 记录交换区的fd */
    cur->lowest_bit = 1;    /* 设置可用的交换槽（块）的最近的索引 */
    cur->highest_bit = maxblocks - 1;   /* 设置可用的交换槽（块）的最远的索引 */
    cur->cluster_next = 1;  /* 交换区内连续可用的交换槽作为一个簇，这个是记录起始索引 */
    cur->cluster_nr = SWAPFILE_CLUSTER; /* 簇内的可用的交换槽的计数 */
    /* 设置交换区内的可用交换槽的计数 */
    cur->nr_free_blocks = \
    cur->nr_blocks = maxblocks;
    /**
     * 设置交换区的优先级，如果参数flags有指定需要设置指定的优先级，则把flags的前15位
     * 作为该交换区的优先级；否则默认使用局部静态变量least_priority设置优先级
     */
    cur->prio = (flags & SWAP_FLAG_PRIO) ? 
                (flags & SWAP_PRIO_MASK) : least_priority--;
    
    /* 把当前交换区加入到swap_list */
    swap_list_lock();
    for (i = swap_list.head; i >= 0; i = swap_info[i].next) {
        if (cur->prio >= swap_info[i].prio)
            break;
        prev = i;
    }
    cur->next = i;
    if (prev < 0)
        swap_list.head = swap_list.next = cur - swap_info;
    else 
        swap_info[prev].next = cur - swap_info;
    swap_list_unlock();

    /* 因为第0个块就是交换区的元消息，所以设置为不可用槽 */
    cur->swap_map[0] = SWAP_MAP_BAD;
    /* 设置为可写 */
    cur->flags = SWAP_WRITE;

    nr_swap_blocks += maxblocks;
    total_swap_blocks += maxblocks;

    mem_free(swap_header);
    keprint(PRINT_INFO "swapon: finish turning on the swap space!\n");
    goto ret;

free_mem:
    mem_free(swap_header);
close_file:
    kfile_close(fd);
ret:
    return result;
}

/**
 * a page(struct page) is moved from swapout_list to active_list
 */
static inline void __activate_swappage(mem_range_t* range, struct page* page)
{
	if (!TestSetPageLRU(page) && !PageActive(page)) {
		del_page_from_swapout_list(page);
		lru_cache_add(range, page);
	}
}

int alloc_page_to_read
(struct seed_swap_struct* seed_swp, unsigned long type)
{
    unsigned long nr_blocks = seed_swp->nr_blocks;
    unsigned long start_blocks = seed_swp->start_block;
    mem_range_t* range = range_gotten_by_type(MEM_RANGE_USER);
    if (range->free_pages < seed_swp->nr_blocks)
        return -1;

    for (int i = 0; i < nr_blocks; i++) {
        unsigned long page_paddr = mem_node_alloc_pages(1, MEM_NODE_TYPE_USER);
        struct page* page = GetMemMapIndex(page_paddr);
        add_page_to_swapout_list(&seed_swp->page_head, page);
        add_page_to_cache_unique(page, SWP_ENTRY((start_blocks+i), type));
    }
    return 0;
}

static unsigned int set_pte(unsigned long page_paddr, unsigned long prot,
    unsigned long start_vaddr, unsigned int index, unsigned int type) 
{
    pte_t* pte;

    while (1) {
        pte = vir_addr_to_table_entry(start_vaddr);
        if (!(*pte & PAGE_ATTR_PRESENT) && SWP_TYPE(*pte) == type && 
            SWP_INDEX(*pte) == index)
            break;
        start_vaddr += PAGE_SIZE;
    }    
    
    *pte = (page_paddr | prot | PAGE_ATTR_PRESENT);
    swap_sdev_lock(&swap_info[type]);
    swap_info[type].swap_map[index]--;
    swap_sdev_unlock(&swap_info[type]);
    return start_vaddr;
}

static int __try_to_revmove_seed_swp(rbtree_node* node, 
    rbtree_node* NIL, mem_range_t* range, struct swap_info_struct* p)
{
    if (node == NIL) 
        return 0;

    if (__try_to_revmove_seed_swp(node->left, NIL, range, p) == -1)
        return -1;
    else 
        node->left = NIL;

    if (__try_to_revmove_seed_swp(node->right, NIL, range, p) == -1)
        return -1;
    else
        node->right = NIL;

    int result = -1;
    struct page* page;
    list_t* space_node;
    struct seed_swap_struct* seed_swp = GetSeedSwpFromRbn(node);

    mutex_lock(&rev_tree_lock);
    rbtree_node* revmap_node = find_node(&revmap_rbtree, seed_swp->revmap_id);
    mutex_unlock(&rev_tree_lock);
    if (!revmap_node) 
        goto del_seed_swp;
    rev_mapping* revmap = GetRevMapFromTree(revmap_node);
    unsigned int type = (unsigned int)(p - swap_info);
    int i, read = 0, shared = 0;

    mutex_lock(&range->swapout_lock);
    if (list_find(&seed_swp->sendswap_node, &range->swap_out_list)) {
        list_del(&seed_swp->sendswap_node);
        if (list_length(&seed_swp->page_head) == seed_swp->nr_blocks) {
            mutex_unlock(&range->swapout_lock);
            goto swapout_data;
        } else {
            mutex_unlock(&range->swapout_lock);

            mutex_lock(&range->lru_lock);
            list_t* entry;
            while ((entry = seed_swp->page_head.next) != &seed_swp->page_head) 
                __activate_swappage(range, GetPageFromLru(entry));
            mutex_unlock(&range->lru_lock);
            goto del_seed_swp;
        }
    } 
    mutex_unlock(&range->swapout_lock);

    if (list_find(&swapout_list, &range->swap_out_list)) 
        goto ret;
    
    if (list_empty(&seed_swp->page_head)) {
        if (!find_page_by_index(SWP_ENTRY(seed_swp->start_block, type))) {
            if (alloc_page_to_read(seed_swp, type) == -1) {
                keprint(PRINT_ERR "swapoff: in the user range there is no enough page to swapin.\n");
                read = 1;
                goto ret;
            }
        } else {
            shared = 1;
        }
    } else {
        goto remove_page_from_cache;
    }

swapout_data:
    if (shared)
        space_node = revmap->rev_map_node->next;
    else
        space_node = revmap->rev_map_node;
    mem_space_t* cur_space = GetSpaceFromNode(space_node);
    unsigned int prot = 0;
    prot |= (cur_space->page_prot & PROT_USER) ? PAGE_ATTR_USER : PAGE_ATTR_SYSTEM;
    prot |= (cur_space->page_prot & PROT_WRITE) ? PAGE_ATTR_WRITE : PAGE_ATTR_READ;
    if (read)
        kfile_lseek(p->fd, seed_swp->start_block*PAGE_SIZE, SEEK_SET);
    do {
        cur_space = GetSpaceFromNode(space_node);
        unsigned long start = seed_swp->index_space*PAGE_SIZE + cur_space->start;
        space_node = space_node->next;

        vmm_active(cur_space->vmm);
        for (i = 0; i < seed_swp->nr_blocks; i++) {
            page = find_page_by_index(SWP_ENTRY((seed_swp->start_block+i), type));
            unsigned long page_paddr = GetPagePhyAdd(page);
            start = set_pte(page_paddr, prot, start, seed_swp->start_block+i, type);
            if (read) {
                page->rev_mapping = cur_space->rev_map;
                kfile_read(p->fd, (void*)start, PAGE_SIZE);
            }
            __activate_swappage(range, page);
            start += PAGE_SIZE;
        }
        vmm_active(NULL);
        read = 0;
    } while (space_node != revmap->rev_map_node);

remove_page_from_cache:
    for (int i = 0; i < seed_swp->nr_blocks; i++) {
        page = find_page_by_index(SWP_ENTRY((seed_swp->start_block+i), type));
        remove_page_from_cache_unique(page);
    }

del_seed_swp:
    swap_free(seed_swp, p);
    mem_free(seed_swp);
ret:
    return result;
}

inline static int try_to_remove_seed_swp(struct swap_info_struct* p)
{
    mem_range_t* range = range_gotten_by_type(MEM_RANGE_USER);
    return __try_to_revmove_seed_swp(p->sendswap_root.root, &p->sendswap_root.NIL, range, p);
}

/**
 * 禁用交换区
 */
int sys_swapoff(const char* swapfile) 
{
    int fd, i, result = 0;

    if (!strcmp(swapfile, swapfile_path)) {
        result = -EDFSWP;
        goto ret;
    }

    if ((kfile_filecmp(swapfile, &fd)) <= 0) {
        result = -ENOPEN;
        goto ret;
    }

    int prev = -1;
    swap_list_lock();
    for (i = swap_list.head; i != -1; i = swap_info[i].next) {
        if (fd == swap_info[i].fd)
            break;
        prev = i;
    }
    if (i == -1) {
        result = -ENSWPS;
        goto unlock;
    } else 
        swap_info[prev].next = swap_info[i].next;
    
    if (prev > 0)
        swap_list.next = &swap_info[prev] - swap_info;

    swap_list_unlock();

    struct swap_info_struct* p = &swap_info[i];
    swap_sdev_lock(p);
    if (p->flags & SWAP_WRITING) {
        result = -EWRITING;
        goto swapsdev_unlock;
    }
    p->flags = SWAP_USED;

    if (try_to_remove_seed_swp(p) == -1) {
        result = -EINSWPL;
        goto adjust_rb_tree;
    }

    p->flags = 0;
    mem_free(p->swap_map);
    kfile_close(p->fd);
    swap_sdev_unlock(p);
    keprint(PRINT_INFO "swapoff: the swap space has been closed.\n");
    return result;

    rbtree_node* node;
adjust_rb_tree:
    node = get_smallest_node(p->sendswap_root.root, &p->sendswap_root);
    if (node->parent && node->parent->color == RB_BLACK) {
        if (node->color == RB_RED)
            node->color = RB_BLACK;
        else 
            delete_case(node, &p->sendswap_root);
    }
    p->flags |= SWAP_WRITE;
swapsdev_unlock:
    swap_sdev_unlock(p);

    prev = -1;
    swap_list_lock();
    for (i = swap_list.head; i >= 0; i = swap_info[i].next) {
        if (p->prio >= swap_info[i].prio)
            break;
        prev = i;
    }
    p->next = i;
    if (prev < 0)
        swap_list.head = swap_list.next = p - swap_info;
    else 
        swap_info[prev].next = p - swap_info;
unlock:
    swap_list_unlock();
ret:
    return result;
}

static inline int scan_swap_map(struct swap_info_struct* p, 
    unsigned int count, struct swap_entry_cluster* curswap_cluster)
{
    unsigned long offset, off_swp, i = 0;
    unsigned int nr_blocks, cur_nr, off = 0;
    int result = 0;
get_blocks:
    if (p->cluster_nr >= count) {
    loop:
        if (p->cluster_next + count - 1 <= p->highest_bit) {
            offset = p->cluster_next;
            for (i = 0; i < count; i++) {
                if (p->swap_map[offset + i]) {
                    p->cluster_next += (i + 1);
                    goto loop;
                }
            }
            p->cluster_next += count;
            p->cluster_nr -= count;
            goto set_swap_map;
        }
    }
    p->cluster_nr = SWAPFILE_CLUSTER;

    offset = p->lowest_bit;
check_next_cluster:
    off_swp = offset + SWAPFILE_CLUSTER;
    if (off_swp - 1 <= p->highest_bit) {
        for (int nr = offset; nr < off_swp; nr++) 
            if (p->swap_map[nr]) {
                offset = nr + 1;
                goto check_next_cluster;
            }

        p->cluster_next = offset;
        goto get_blocks;
    }

    /* 最坏的情况 */
    nr_blocks = 0, cur_nr = 0;
    offset = p->lowest_bit;
loop2:
    if (offset + count - 1 <= p->highest_bit) {
        for (i = 0; i < count; i++) {
            if (p->swap_map[offset + i]) {
                offset += i + 1;
                if (nr_blocks < cur_nr) {
                    nr_blocks = cur_nr;
                    cur_nr = 0;
                    off = offset;
                }
                goto loop2;
            }
            cur_nr++;
        }
    }
    if (i <= count) {
        count = nr_blocks; offset = off; result = -1;
    }
    p->cluster_next = offset + count;

set_swap_map:
    if (offset == p->lowest_bit)
        p->lowest_bit += count;
    
    if ((offset + count-1) == p->highest_bit) {
        unsigned int highest_bit = p->highest_bit - count;
        unsigned int lowest_bit = p->lowest_bit;
        unsigned char* swap_map = p->swap_map;
        for (; highest_bit >= lowest_bit && 
            swap_map[highest_bit]; highest_bit--);
    }

    if (p->lowest_bit >= p->highest_bit) {
        p->lowest_bit = p->nr_blocks;
        p->highest_bit = 1;
    }

    for (int i = 0; i < count; i++) 
        p->swap_map[offset + i] = 1;
    p->nr_free_blocks -= count;
    nr_swap_blocks -= count;
    curswap_cluster->index = offset;
    curswap_cluster->count = count;
    return result;
}

void get_swap_block
(unsigned int count, struct swap_entry_cluster* array)
{
    struct swap_info_struct *p;
    unsigned long result, index = 0;
    int type, wrapped = 0;

    swap_list_lock();
    type = swap_list.next;
    if (type < 0 || nr_swap_blocks <= 0)
        goto out;
    
    while (1) {
        p = &swap_info[type];
        if (p->flags & SWAP_WRITE) {
            swap_sdev_lock(p);
            result = scan_swap_map(p, count, &array[index]);
            swap_sdev_unlock(p);
            if (unlikely((result == -1))) {
                if (array[index].count)
                    index++;  
            } else {
                array[index].type = type;
                type = swap_info[type].next;
                if (type < 0 || p->prio != swap_info[type].prio)
                    swap_list.next = swap_list.head;
                else {
                    swap_list.next = type;
                }
                goto out;
            }
        }

        type = p->next;
        if (!wrapped) {
            if (type < 0 || p->prio != swap_info[type].prio) {
                type = swap_list.head;
                wrapped = 1;
            }
        } else {
            if (type < 0)
                goto out;
        }
    }

out:
    swap_list_unlock();
}

void swap_free(struct seed_swap_struct* seed_swp, struct swap_info_struct* p)
{
    unsigned long start = seed_swp->start_block;
    unsigned long nr_blocks = seed_swp->nr_blocks;

    swap_sdev_lock(p);
    while (nr_blocks--) {
        p->swap_map[start++] = 0;
    }

    if (seed_swp->start_block < p->lowest_bit)
        p->lowest_bit = seed_swp->start_block;
    
    if (start-1 > p->highest_bit)
        p->highest_bit = start-1;

    p->nr_free_blocks += seed_swp->nr_blocks;
    nr_swap_blocks += seed_swp->nr_blocks;
    swap_sdev_unlock(p);
}

void __del_node_from_swapinfo
(rbtree_node* node, rbtree_root* rbtree, struct swap_info_struct* p)
{
    struct seed_swap_struct* seed_swap;

    if (node == &rbtree->NIL)
        return;
    
again:
    seed_swap = GetSeedSwpFromRbn(node);
    mutex_lock(&rev_tree_lock);
    rbtree_node* revmap_node = find_node(&revmap_rbtree, seed_swap->revmap_id);
    mutex_unlock(&rev_tree_lock);
    /* 反映射已被释放 */
    if (!revmap_node)
        goto next;

    /**
     * 子区的物理页也被释放，且子区不在range待换出链表 
     */
    if (list_empty(&seed_swap->page_head) &&
        list_empty(&seed_swap->sendswap_node)) {
        swap_free(seed_swap, p);
        node = rbtree_delete(node, rbtree);
        mem_free(seed_swap);
        if (node != &rbtree->NIL)
            goto again;
    }

next:
    __del_node_from_swapinfo(node->left, rbtree, p);
    __del_node_from_swapinfo(node->right, rbtree, p);
}

static inline void del_node_from_swapinfo(struct swap_info_struct* p) 
{  
    if (p->nr_blocks/2 >= p->nr_free_blocks)
        __del_node_from_swapinfo(p->sendswap_root.root, &p->sendswap_root, p);
}

static void adjust_swap_space()
{
    int type;
    swap_list_lock();
    for (type = swap_list.head; type != -1; type = swap_info[type].next) {
        swap_sdev_lock(&swap_info[type]);
        del_node_from_swapinfo(&swap_info[type]);
        swap_sdev_unlock(&swap_info[type]);
    }
       
    swap_list_unlock();
}

static inline struct swap_info_struct* 
get_swapinfo_from_page(list_t* page_node)
{
    struct page* page = GetPageFromLru(page_node);
    return &swap_info[SWP_TYPE(page->swp_entry)];
}

/**
 * 内存换出的关键函数
 */
void write_swap_page()
{
    mem_range_t* range = range_gotten_by_type(MEM_RANGE_USER);
    list_init(&swapout_list);
    while (1) {
        /**
         * 在user区域的swapout_list找出可换出的子区
         */
        mutex_lock(&range->swapout_lock);
        if (!list_empty(&range->swap_out_list)) {
            list_replace_init(&range->swap_out_list, &swapout_list);
            range->nr_swapout_pages = 0;
            mutex_unlock(&range->swapout_lock);

            list_t* entry = swapout_list.next;
            while(entry != &swapout_list) {
                struct seed_swap_struct* seed_swp = GetSeedSwpFromNode(entry);
                list_t* page_list = &seed_swp->page_head;
                entry = entry->next;
                /**
                 * 如果块的数量和物理页的数量不相等，则说明这个页已经被释放了，且极大可能
                 * space已被释放。这个子区已没有换出的价值，换下一个。  
                 */
                if (list_length(page_list) != seed_swp->nr_blocks)
                    goto next;
                
                 /* 情况上面写的一样*/
                mutex_lock(&rev_tree_lock);
                rbtree_node* rbnode = find_node(&revmap_rbtree, seed_swp->revmap_id);
                mutex_unlock(&rev_tree_lock);
                if (likely(rbnode)) {
                    struct swap_info_struct* p = get_swapinfo_from_page(page_list->next);
                    swap_sdev_lock(p);
                    p->flags &= SWAP_WRITING;

                    unsigned char* swap_map = p->swap_map;
                    /* 因为子区的块是连续的，所以定义一个nr_blocks个块的缓冲区 */
                    char* buffer = (char*)mem_alloc(PAGE_SIZE * seed_swp->nr_blocks);
                    list_t* page_entry;
                    struct page* page;
                    int read = 1;
                    int buf_index = 0;

                    /**
                     * usually just loop once as most spaces is private
                     * 通常只循环一次，因为大多数是私有的
                     */
                    rev_mapping* cur_revmap = GetRevMapFromTree(rbnode);
                    mutex_lock(&cur_revmap->revmap_lock);
                    list_t* space_node = cur_revmap->rev_map_node;
                    do {
                        mem_space_t* cur_space = GetSpaceFromNode(space_node);
                        space_node = space_node->next;

                        page_entry = page_list->next;
                        vmm_active(cur_space->vmm);
                        while (page_entry != page_list) {
                            page = GetPageFromLru(page_entry);
                            page_entry = page_entry->next;
                            
                            SetPageLaunder(page);
                            unsigned long vaddr = cur_space->start + page->index * PAGE_SIZE;
                            swap_map[SWP_INDEX(page->swp_entry)]++;

                            if (read) {
                                memcpy(buffer+buf_index, (void*)vaddr, PAGE_SIZE);
                                buf_index += PAGE_SIZE;
                            }
                                
                            pte_t* pte = vir_addr_to_table_entry(vaddr);
                            *pte = page->swp_entry;
                        }
                        vmm_active(NULL);
                        read = 0;
                    } while (space_node != cur_revmap->rev_map_node);
                    mutex_unlock(&cur_revmap->revmap_lock);

                    kfile_lseek(p->fd, seed_swp->start_block*PAGE_SIZE, SEEK_SET);
                    kfile_write(p->fd, buffer, PAGE_SIZE * seed_swp->nr_blocks);
                    
                    p->flags &= ~SWAP_WRITING;
                    mem_free(buffer);
                    swap_sdev_unlock(p);

                    /* prevent page-fault from being called */
                    /**
                     * 当上部分执行完成之后，如果执行下部分的代码前因页面换出导致发出缺页
                     * 中断，则把该子区的页全部从该链表移除，重新加入到活跃链表。此时该链
                     * 表无节点，这样也释放不了，可以继续使用。
                     * 如果已经执行下部分的代码再发出缺页中断，则需要新分配页再从磁盘读出。
                     */
                    unsigned long flags;
	                interrupt_save_and_disable(flags);
                    page_entry = page_list->next;
                    while (page_entry != page_list) {
                        page = GetPageFromLru(page_entry);
                        page_entry = page_entry->next;
                        del_page_from_swapout_list(page);
                        remove_page_from_cache_unique(page);
                        mem_node_free_pages(GetPagePhyAdd(page));
                        StructPageInit(page);
                    } 
                    interrupt_restore_state(flags);
            #ifdef DEBUG_SWAP
                    keprint(PRINT_INFO "swap: the pages in the seed swap has been swapped out to the swap space!\n");
            #endif
                }
            next:
                list_del(entry->prev);
            }
        } else {
            mutex_unlock(&range->swapout_lock);
            task_sleep();
        }
    }
}

/**
 * 如果因为内存交换发出缺页中断，完成之后，页描述符还被记录在swap cache当中。
 * 目的是允许该页还可以在对应的槽 
 */
void activate_swappage(unsigned int type, struct seed_swap_struct* seed_swp, 
                       mem_space_t* space, int read)
{
    unsigned long page_paddr;
    unsigned int start_index = seed_swp->start_block;
    unsigned int start = space->start + seed_swp->index_space*PAGE_SIZE;
    unsigned int prot = 0;
    prot |= (space->page_prot & PROT_USER) ? PAGE_ATTR_USER : PAGE_ATTR_SYSTEM;
    prot |= (space->page_prot & PROT_WRITE) ? PAGE_ATTR_WRITE : PAGE_ATTR_READ;
    struct page* page;

    if (!list_empty(&seed_swp->page_head)) {
        if (list_length(&seed_swp->page_head) != seed_swp->nr_blocks)
            panic("do_swap_fault: the count of the list is less than the nr_blocks.\b");
        mem_range_t* range = range_gotten_by_type(MEM_RANGE_USER);
        list_t* page_node;
        int fd;

        if (read) {
            fd = swap_info[type].fd; 
            kfile_lseek(fd, start_index*PAGE_SIZE, SEEK_SET);
        }
        while ((page_node = seed_swp->page_head.next) != &seed_swp->page_head) {
            page = GetPageFromLru(page_node);
            page_paddr = GetPagePhyAdd(page);
            start = set_pte(page_paddr, prot, start, start_index, type);
            if (read) {
                page->rev_mapping = space->rev_map;
                page->index = (start - space->start) / PAGE_SIZE;
                page->swp_entry = SWP_ENTRY(start_index, type);
                kfile_read(fd, (void*)start, PAGE_SIZE);
            }
            __activate_swappage(range, page);
            start_index++;
            start += PAGE_SIZE;
        }
    } else {
        for (int i = 0; i < seed_swp->nr_blocks; i++) {
            page = find_page_by_index(SWP_ENTRY(start_index, type));
            page_paddr = GetPagePhyAdd(page);
            start = set_pte(page_paddr, prot, start, start_index, type);
            start_index++;
            start += PAGE_SIZE;
        }
    }

    /**
     * if the rev_mapping is not there, the swap blocks in swap_send are
     * freed and swap_send is removed from the rb_tree and freed
     */
    adjust_swap_space();
}
