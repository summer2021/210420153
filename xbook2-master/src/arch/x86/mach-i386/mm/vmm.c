#include <arch/page.h>
#include <arch/vmm.h>
#include <xbook/memalloc.h>
#include <xbook/debug.h>
#include <xbook/memspace.h>
#include <xbook/vmm.h>
#include <xbook/schedule.h>
#include <xbook/lru.h>
#include <xbook/mm.h>
#include <arch/mempool.h>
#include <arch/tss.h>
#include <string.h>

#define DEBUG_VMM

static int do_copy_share_page(addr_t vaddr, vmm_t *child, vmm_t *parent)
{
    addr_t paddr = addr_vir2phy(vaddr);
    dbgprint("[vmm]: copy share page at vaddr %x phy addr %x\n", vaddr, paddr);   
    vmm_active(child);
    keprint(PRINT_DEBUG "do_copy_share_page: va:%d, pa:%d\n", vaddr, paddr);
    page_link_addr(vaddr, paddr, PAGE_ATTR_WRITE | PAGE_ATTR_USER);
    vmm_active(parent);
    return 0;
}

static int do_copy_normal_page(addr_t vaddr, rev_mapping* revmap, 
                int index, void *buf, vmm_t *child, vmm_t *parent)
{
    addr_t paddr;
    memcpy(buf, (void *)vaddr, PAGE_SIZE);
    vmm_active(child);
    paddr = page_alloc_user(1);
    if (!paddr) {
        keprint(PRINT_ERR "vmm_copy_mapping: page_alloc_one for vaddr failed!\n");
        vmm_active(parent);
        return -1;
    }
    page_link_addr(vaddr, paddr, PAGE_ATTR_WRITE | PAGE_ATTR_USER);
    memcpy((void *)vaddr, buf, PAGE_SIZE);
    page_add_revmap(paddr, index, revmap);
    /* LRU */
    lru_add_by_range_type_irqsave(MEM_RANGE_USER, paddr);
    vmm_active(parent);
    return 0;
}

int vmm_copy_mapping(task_t *child, task_t *parent)
{
    void *buf = mem_alloc(PAGE_SIZE);
    if (buf == NULL) {
        keprint(PRINT_ERR "vmm_copy_mapping: mem_alloc buf for data transform failed!\n");
        return -1;
    }
    mem_space_t *par_space = parent->vmm->mem_space_head;
    mem_space_t *chi_space = child->vmm->mem_space_head;
    addr_t prog_vaddr = 0;
    int index;
    while (par_space) {
        //keprint(PRINT_WARING "\nchi_space->rev_map->rb_node.value:%d\n", chi_space->rev_map->rb_node.value);
        prog_vaddr = par_space->start;
        index = 0;
        while (prog_vaddr < par_space->end) {
            /* 如果是共享内存，就只复制页映射，而不创建新的页(映射空间) */
            if (par_space->flags & MEM_SPACE_MAP_SHARED) {
                if (do_copy_share_page(prog_vaddr, child->vmm, parent->vmm) < 0) {
                    mem_free(buf);
                    return -1;
                }
            } else {
                /* 不是共享内存，比如：代码段、数据段、堆段、栈段 */
                if (do_copy_normal_page(prog_vaddr, chi_space->rev_map, index++, 
                    buf, child->vmm, parent->vmm) < 0) {
                    mem_free(buf);
                    return -1;
                }
            }

            prog_vaddr += PAGE_SIZE;
        }
        par_space = par_space->next;
        chi_space = chi_space->next;
    }
    mem_free(buf);
    return 0; 
}

void vmm_active_kernel()
{
    unsigned long paddr = KERN_PAGE_DIR_PHY_ADDR;
    cpu_cr3_write(paddr);
    tss_update_info((unsigned long )task_current);
}

void vmm_active_user(unsigned int page)
{
    cpu_cr3_write(page);    
    tss_update_info((unsigned long )task_current);
}