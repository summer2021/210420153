#include <xbook/debug.h>
#include <xbook/memcache.h>
#include <xbook/memspace.h>
#include <xbook/mm.h>

rbtree_root revmap_rbtree;
mutexlock_t rev_tree_lock;
static unsigned int revmap_id = 0;

inline void 
add_revmap_node_to_list(rev_mapping* revmap, mem_space_t* space)
{
    mutex_lock(&revmap->revmap_lock);
    list_add(revmap->rev_map_node, &space->rev_map_node);
    mutex_unlock(&revmap->revmap_lock);
}

inline void 
del_revmap_node_from_list(rev_mapping* revmap, mem_space_t* space)
{
    mutex_lock(&revmap->revmap_lock);
    list_del(&space->rev_map_node);
    mutex_unlock(&revmap->revmap_lock);
}

inline void add_revmap_to_rbtree(rev_mapping* revmap)
{
    mutex_lock(&rev_tree_lock);
    rbtree_insert(&revmap->rb_node, &revmap_rbtree);
    mutex_unlock(&rev_tree_lock);
}

inline void del_revmap_from_rbtree(rev_mapping* revmap)
{
    mutex_lock(&rev_tree_lock);
    rbtree_delete(&revmap->rb_node, &revmap_rbtree);
    mutex_unlock(&rev_tree_lock);
}

/**
 * 分配一个新的rev_mapping，并把space加入其中
 */
void alloc_revmapping(mem_space_t* space)
{
    rev_mapping* revmap;
    if (!(revmap = (rev_mapping*)mem_alloc(sizeof(rev_mapping)))) {
        keprint(PRINT_ERR "alloc_revmaping: failed to build rev_mapping as there is no free memory");
        return;
    }
    rev_mapping_init(revmap);

    add_revmap_to_rbtree(revmap);
    revmap->rev_map_node = &space->rev_map_node;
    space->rev_map = revmap;
}

/**
 * 尝试删除rev_mapping，如果rev_mapping链表只有一个节点，则
 * 删除rev_mapping，否则仅删除节点
 */
void try_to_del_revmapping(mem_space_t* space)
{
    rev_mapping* revmap = space->rev_map; 
    if (list_empty(revmap->rev_map_node)) {
        del_revmap_from_rbtree(revmap);
        mem_free(revmap);
        return;
    } else {
        del_revmap_node_from_list(revmap, space);
    }
}

/**
 * 这个是给带有共享flags的space使用，根据paddr找到相应的反映射，如果有
 * 就表示先前已有共享空间，加入该反映射即可。如果没有则新建一个反映射并加
 * 入。 
 */
void try_adding_to_revmap(unsigned long paddr, mem_space_t* space)
{
    struct page* page = GetMemMapIndex(paddr);

    if (page->rev_mapping) {
        add_revmap_node_to_list(page->rev_mapping, space);
        space->rev_map = page->rev_mapping;
    } else {
        alloc_revmapping(space);
    }
}

/**
 * 给page设置在空间内的偏移（以页为单位）
 */
inline void 
page_add_revmap(unsigned long paddr, int index, rev_mapping* revmap)
{
    struct page* page = GetMemMapIndex(paddr);
    page->index = index;
    page->rev_mapping = revmap;
}

inline void rev_mapping_init(rev_mapping* revmap)
{
    mutexlock_init(&revmap->revmap_lock);
    revmap->rev_map_node = NULL;
    init_rbtree_node(&revmap->rb_node, revmap_id, &revmap_rbtree);
    revmap_id++;
}

void revmap_init()
{
    init_rbtree(&revmap_rbtree);
    mutexlock_init(&rev_tree_lock);
}
