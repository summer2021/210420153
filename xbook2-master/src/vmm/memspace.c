#include <xbook/memspace.h>
#include <xbook/task.h>
#include <xbook/debug.h>
#include <xbook/schedule.h>
#include <xbook/mm.h>

// #define DEBUG_MEM_SPACE
// #define DEBUG_REV_MAPPING

/**
 * 查看一个用户空间的所有的段（mem_space）的元信息
 */
void mem_space_dump(vmm_t *vmm)
{
    if (!vmm)
        return;
    mem_space_t *space = vmm->mem_space_head;
    while (space) {
        keprint(PRINT_INFO "space: start=%x end=%x prot=%x flags:%x\n", 
            space->start, space->end, space->page_prot, space->flags);
        space = space->next;
    }
}

/**
 * 在VMM插入一个新space，并尝试合并
 */
static int mem_space_insert(vmm_t *vmm, mem_space_t **space, unsigned long paddr)
{
    int nr_pages, aligned_pages_size;
    int index_inspace = 0;
    struct page *page;
    mem_space_t *cur_space = *space;
    mem_space_t *prev = NULL;
    mem_space_t *p = (mem_space_t *)vmm->mem_space_head;
    while (p) {
        if (cur_space->end <= p->start)
            break;
        prev = p;
        p = p->next;
    }
    cur_space->next = p;
    if (prev)
        prev->next = cur_space;
    else
        vmm->mem_space_head = (void*)cur_space;
    cur_space->vmm = vmm;

    /**
     * 共享内存不进行合并处理
     * 根据paddr对应的struct page的rev_map是否为空来判断是否是第
     * 一次设置共享空间，为空表示第一次设置，所以新建一个反映射结构体
     * ，并把space加入在内；不为空则加入。
     */
    if (cur_space->flags & MEM_SPACE_MAP_SHARED) {
        try_adding_to_revmap(paddr, cur_space);
        return 0;
    }
        
    nr_pages = (cur_space->end - cur_space->start) / PAGE_SIZE;

    /**
     * merge prev and space
     * 如果当前的space的start_vaddr和上一个space_end_vaddr相等，
     * 且属性相同，则把它们两融合在一起，即仅用一个mem_space来描述这
     * 这个属性相同的段
     */
    if (prev && prev->end == cur_space->start) {
        if ((prev->page_prot == cur_space->page_prot) && 
            (prev->flags == cur_space->flags)) 
        {
            index_inspace = (prev->end - prev->start) / PAGE_SIZE;
            
            nr_pages += index_inspace;
            prev->end = cur_space->end;
            prev->next = p;
            mem_space_free(cur_space);
            *space = cur_space = prev;
        }
    } else {
        alloc_revmapping(cur_space);
    }

    /**
     * merge space and p
     * 如果当前的space的end_vaddr和下一个space的start_vaddr相等，
     * 且属性相同，则把它们两融合在一起，即仅用一个mem_space来描述这
     * 这个属性相同的段
     */
    if (p && cur_space->end == p->start) {
        if ((cur_space->page_prot == p->page_prot) && 
            (cur_space->flags == p->flags)) 
        {
            aligned_pages_size = (p->end - p->start) / PAGE_SIZE;
            page = GetMemMapIndex(addr_vir2phy(p->start));
            for(int i = 0; i < aligned_pages_size; i++) {
                page[i].rev_mapping = cur_space->rev_map;
                page[i].index += nr_pages;
            }
            
            try_to_del_revmapping(p);
            cur_space->end = p->end;
            cur_space->next = p->next;
            mem_space_free(p);    
        }
    }
    return index_inspace;
}

/**
 * 在VMM指定的映射空间范围内找出一个大小为len的空闲空间 
 */
unsigned long mem_space_get_unmaped(vmm_t *vmm, unsigned len)
{
    unsigned long addr = vmm->map_start;
    mem_space_t *space = mem_space_find(vmm, addr);
    while (space) {
        /* 如果addr+len超过了用户空间 */
        /*if (USER_VMM_SIZE - len < addr + USER_VMM_BASE_ADDR) {
            errprint("mem_space_get_unmaped: len too big!\n");
            return -1;
        }*/

        if (addr + len >= vmm->map_end)
            return -1;
        if (addr + len <= space->start)
            return addr;
    
        addr = space->end;
        space = space->next;
    }
    return addr+len < vmm->map_end ? addr : -1;
}

int do_mem_space_map(vmm_t *vmm, unsigned long addr, unsigned long paddr, 
    unsigned long len, unsigned long prot, unsigned long flags)
{	
    if (vmm == NULL || !prot) {
        keprint(PRINT_ERR "do_mem_space_map: failed!\n");
        return -1;
    }
    len = PAGE_ALIGN(len);
    if (!len) {
        keprint(PRINT_ERR "do_mem_space_map: len is zero!\n");
        return -1;
    }
    if (len > USER_VMM_SIZE || addr > USER_VMM_TOP_ADDR || 
        addr > USER_VMM_TOP_ADDR - len || addr < USER_VMM_BASE_ADDR) {
        keprint(PRINT_ERR "do_mem_space_map: addr %x and len %x out of range!\n", addr, len);
        return -1;
    }

    if (flags & MEM_SPACE_MAP_FIXED) {
        /* 检查addr是否是页对齐 */
        if (addr & ~PAGE_MASK) {
            keprint(PRINT_ERR "do_mem_space_map: addr %x not page aligined!\n", addr);
            return -1;
        }
        mem_space_t* p = mem_space_find(vmm, addr);
        if (p && addr + len > p->start) {
            keprint(PRINT_ERR "do_mem_space_map: the FIXED space has existed!\n");
            return -1;
        }
    } else {
        addr = mem_space_get_unmaped(vmm, len);
        if (addr == -1) {
            keprint(PRINT_ERR "do_mem_space_map: GetUnmappedMEM_SPACEpace failed!\n");
            return -1;
        }
    }

    if (flags & MEM_SPACE_MAP_REMAP) {
        prot |= PROT_REMAP;
    }

    mem_space_t *space = mem_space_alloc();
    if (!space) {
        keprint(PRINT_ERR "do_mem_space_map: mem_alloc for space failed!\n");
        return -1;    
    }
    mem_space_init(space, addr, addr + len, prot, flags);
    int start_index = mem_space_insert(vmm, &space, paddr);

    /* 如果是共享映射，就映射成共享的地址，需要指定物理地址 */
    if (flags & MEM_SPACE_MAP_SHARED) {
        page_map_addr_fixed(addr, paddr, len, prot);
        for (int i = 0, j = 0; i < len; i += PAGE_SIZE, j++) {
            page_add_revmap(paddr + i, j, space->rev_map);
#ifdef DEBUG_REV_MAPPING
            keprint(PRINT_DEBUG "shared_space: page->index: %d\n", 
                    GetMemMapIndex((paddr + i))->index);
#endif            
        }
    } else {
        page_map_addr_safe(addr, len, prot);
        unsigned long vstart = space->start;
        for (int i = 0; i < len; i += PAGE_SIZE) {
            page_add_revmap(addr_vir2phy(vstart + i), start_index++, space->rev_map);
#ifdef DEBUG_REV_MAPPING
            keprint(PRINT_DEBUG "fixed_space: revmap_id: %d\n", 
                    GetMemMapIndex(addr_vir2phy(vstart + i))->rev_mapping->rb_node.value);
            keprint(PRINT_DEBUG "fixed_space: page->index: %d\n", 
                    GetMemMapIndex(addr_vir2phy(vstart + i))->index);
#endif
        }
    }
    return addr;
}

/**
 * 该函数和上面的函数类似，只不过设置共享的空间时，vaddr需要转换为物理地址，
 * 再把物理地址写入到该空间的相应的页表当中。
 */
int do_mem_space_map_viraddr(vmm_t *vmm, unsigned long addr, unsigned long vaddr, 
    unsigned long len, unsigned long prot, unsigned long flags)
{
    if (vmm == NULL || !prot) {
        keprint(PRINT_ERR "do_mem_space_map: failed!\n");
        return -1;
    }
    len = PAGE_ALIGN(len);
    if (!len) {
        keprint(PRINT_ERR "do_mem_space_map: len is zero!\n");
        return -1;
    }
    if (len > USER_VMM_SIZE || addr > USER_VMM_TOP_ADDR || 
        addr > USER_VMM_TOP_ADDR - len || addr < USER_VMM_BASE_ADDR) {
        keprint(PRINT_ERR "do_mem_space_map_viraddr: addr %x and len %x out of range!\n", addr, len);
        return -1;
    }
    if (flags & MEM_SPACE_MAP_FIXED) {
        if (addr & ~PAGE_MASK)
            return -1;
        mem_space_t* p = mem_space_find(vmm, addr);
        if (p != NULL && addr + len > p->start) {
            keprint(PRINT_ERR "do_mem_space_map: this FIXED space had existed!\n");
            return -1;
        }
    } else {
        addr = mem_space_get_unmaped(vmm, len);
        if (addr == -1) {
            keprint(PRINT_ERR "do_mem_space_map: GetUnmappedMEM_SPACEpace failed!\n");
            return -1;
        }
    }
    if (flags & MEM_SPACE_MAP_REMAP) {
        prot |= PROT_REMAP;
    }
    mem_space_t *space = mem_space_alloc();
    if (!space) {
        keprint(PRINT_ERR "do_mem_space_map: mem_alloc for space failed!\n");
        return -1;    
    }
    mem_space_init(space, addr, addr + len, prot, flags);
    mem_space_insert(vmm, &space, kern_vir_addr2phy_addr(vaddr));
    /* 如果是共享映射，就映射成共享的地址，需要指定物理地址 */
    if (flags & MEM_SPACE_MAP_SHARED) {
        /* 单页映射，避免虚拟地址不连续造成的映射问题 */
        unsigned long vend = vaddr + len;
        unsigned long paddr;
        unsigned long vstart = addr;
        unsigned long index = 0;
        while (vaddr < vend) {
            paddr = kern_vir_addr2phy_addr(vaddr);
            page_map_addr_fixed(vstart, paddr, PAGE_SIZE, prot);
            page_add_revmap(paddr, index++, space->rev_map);
#ifdef DEBUG_REV_MAPPING
            keprint(PRINT_DEBUG "vir_shared_space: page->index: %d\n", 
                    GetMemMapIndex(paddr)->index);
#endif
            vaddr += PAGE_SIZE;
            vstart += PAGE_SIZE;
        }
    } /*else {
        page_map_addr_safe(addr, len, prot); 
    }*/
    return addr;
}

int do_mem_space_unmap(vmm_t *vmm, unsigned long addr, unsigned long len)
{
    if ((addr & ~PAGE_MASK) || addr > USER_VMM_TOP_ADDR || 
        addr > USER_VMM_TOP_ADDR - len || addr < USER_VMM_BASE_ADDR) {
        keprint(PRINT_ERR "do_mem_space_unmap: addr %x and len %x error!\n", addr, len);
        return -1;
    }
    len = PAGE_ALIGN(len);
    if (!len) {
        keprint(PRINT_ERR "do_mem_space_unmap: len is zero!\n");
        return -1;
    }

    mem_space_t* prev = NULL;
    mem_space_t* space = mem_space_find_prev(vmm, addr, &prev);
    if (space == NULL) {      
        keprint(PRINT_ERR "do_mem_space_unmap: not found the space!\n");
        return -1;
    }
    
    if (addr < space->start || addr + len > space->end) {
        // 不在范围内，需要进行控件拓展
        // noteprint("unmap: addr out of range: addr%x -> [%x-%x]\n", addr, space->start, space->end);
        return 0;
    }
    page_unmap_addr_safe(addr, len, space->flags & MEM_SPACE_MAP_SHARED);

    mem_space_t* space_new = mem_space_alloc();
    if (!space_new) {        
        keprint(PRINT_ERR "do_mem_space_unmap: mem_alloc for space_new failed!\n");
        return -1;
    }

    space_new->start = addr + len;
    space_new->end = space->end;
    space->end = addr;
    space_new->next = space->next;
    space->next = space_new;
    unsigned int nr_pages_inspace = (space_new->end - space_new->start) / PAGE_SIZE;

    if (space->start == space->end) {
        try_to_del_revmapping(space);
        mem_space_remove(vmm, space, prev);
        space = prev;
    }

    if (space_new->start == space_new->end) {
        mem_space_remove(vmm, space_new, space);
    } else {
        alloc_revmapping(space_new);
        unsigned long start = space_new->start;
        for (int i = 0; i < nr_pages_inspace; i++)
            page_add_revmap(addr_vir2phy(start + i * PAGE_SIZE), 
                            i, space_new->rev_map);
    }
    return 0;
}

void *mem_space_mmap(uint32_t addr, uint32_t paddr, uint32_t len, uint32_t prot, uint32_t flags)
{
    task_t *current = task_current;
    return (void *)do_mem_space_map(current->vmm, addr, paddr, len, prot, flags);
}

void *mem_space_mmap_viraddr(uint32_t addr, uint32_t vaddr, uint32_t len, uint32_t prot, uint32_t flags)
{
    task_t *current = task_current;
    return (void *)do_mem_space_map_viraddr(current->vmm, addr, vaddr, len, prot, flags);
}

int mem_space_unmmap(uint32_t addr, uint32_t len)
{
    task_t *current = task_current;
    return do_mem_space_unmap(current->vmm, addr, len);
}

/**
 * 如果参数addr是堆空间的结束地址，则直接向上扩展至addr+len即可。
 * 否则就新建一个以addr为起始地址，结束地址为addr+len的堆空间。
 */
static unsigned long 
do_mem_space_expend_heap(vmm_t *vmm, unsigned long addr, unsigned long len)
{
    len = PAGE_ALIGN(len);
    if (!len) 
        return addr;
    mem_space_t *space;
    unsigned long flags, ret;
    if ((ret = do_mem_space_unmap(vmm, addr, len)) == -1)
        return -1;
    flags = MEM_SPACE_MAP_HEAP;

    /**
     * 查找结束地址为addr的堆空间（属性相同），如果找到了说明可以连续，即
     * space->end = addr + len，如果没有则新建一个堆空间并插入到VMM的
     * space单向链表。
     */
    if (addr) {
        space = mem_space_find(vmm, addr - 1);
        if (space && space->end == addr && space->flags == flags) {
            space->end = addr + len;
            goto the_end;
        }
    }
    space = mem_space_alloc();
    if (space == NULL)
        return -1;
    mem_space_init(space, addr, addr + len, PROT_USER | PROT_WRITE | PROT_EXEC, flags);
    mem_space_insert(vmm, &space, 0);
the_end:
    return addr;
}

/**
 * 该函数用于管理堆空间，参数heap是一个分界线，如果space->end大于heap，
 * 则把该空间的上一部分（heap～space->end）释放。如果heap大于space->
 * end, 并且小于堆空间的最大范围，则两个方向：如果heap+PAGE_SIZE与推空
 * 空间的下一个空间相交（heap+PAGE_SIZE > next_space->start）则直接
 * 返回；否则把当前的堆空间向上扩展到heap。
 */
unsigned long sys_mem_space_expend_heap(unsigned long heap)
{
    unsigned long ret;
    unsigned long old_heap, new_heap;
    vmm_t *vmm = task_current->vmm;
#ifdef DEBUG_MEM_SPACE    
    keprint(PRINT_DEBUG "%s: task %s pid %d vmm heap start %x end %x new %x\n", 
        __func__, task_current->name, task_current->pid, vmm->heap_start, vmm->heap_end, heap);
#endif
    /* 如果堆比开始位置都小就退出 */
    if (heap < vmm->heap_start) {
        goto the_end;
    }
    new_heap = PAGE_ALIGN(heap);
    old_heap = PAGE_ALIGN(vmm->heap_end);
    /* 如果在同一个页 */
    if (new_heap == old_heap) {
        goto set_heap; 
    }
    
    if (heap <= vmm->heap_end) {
        if (!do_mem_space_unmap(vmm, new_heap, old_heap - new_heap))
            goto set_heap;
        keprint(PRINT_ERR "sys_mem_space_expend_heap: do_mem_space_unmap failed!\n");
        goto the_end;
    }
    
    if (heap > vmm->heap_start + MAX_MEM_SPACE_HEAP_SIZE) {
        keprint(PRINT_ERR "%s: %x out of heap boundary!\n", __func__, heap);
        goto the_end;
    }

    if (mem_space_find_intersection(vmm, old_heap, new_heap + PAGE_SIZE)) {
        keprint(PRINT_ERR "%s: space intersection! old=%x, new=%x, end=%x\n",
            __func__, old_heap, new_heap, new_heap + PAGE_SIZE);
#ifdef DEBUG_MEM_SPACE   
        keprint(PRINT_ERR "%s: find: start=%x, end=%x\n",
            __func__, find->start, find->end);

        keprint(PRINT_ERR "task=%d.\n", task_current->pid);
#endif
        goto the_end;
    }
    if (do_mem_space_expend_heap(vmm, old_heap, new_heap - old_heap) != old_heap) {
        keprint(PRINT_ERR "sys_mem_space_expend_heap: do_heap failed! addr %x len %x\n",
            old_heap, new_heap - old_heap);
        goto the_end;
    }
set_heap:
#ifdef DEBUG_MEM_SPACE   
    keprint(PRINT_DEBUG "sys_mem_space_expend_heap: set new heap %x old is %x\n",
        heap, vmm->heap_end);
#endif
    vmm->heap_end = heap;
the_end:
    ret = vmm->heap_end;
    return ret;
}
