#include <sys/syscall.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * 错误码
 */
enum err_code {
    /* the file has been opened by the other tasks */
    EOPEN1 = 10,
    /* the file does not exist */
    EOPEN2,
    /* the status of the file cannot be gotten. */
    ESTAT,
    /* free memory cannot be gotten */
    EGMEM,
    /* the data in the file cannot be written in the memory from the disk */
    EREAD,
    /* there is no swap_header in the header of the file */
    ENOSWP,
    /* the size of the file is different from before */
    ESZDIF,
    /* there is no free member in the swap_info array */
    ENINFO,
};

/**
 * 错误码对应的字符串
 */
static const char* erropen1_str = "failed to creat swap space as the file has been opened by the other tasks.";
static const char* erropen2_str = "failed to open this file, please examine the file path.";
static const char* errstat_str = "failed to get the status of the file";
static const char* errgmem_str = "failed to get memory. perhaps there is no free memory.";
static const char* errread_str = "failed to get swap_header from the file in the disk.";
static const char* errnoswp_str = "the file is not a swap space. \n\
        please choose the other file or call mkswap to make it be a new swap space.";
static const char* errszdif_str = "the size of the file is smaller or bigger than before.\n\
        please call mkswap to repair it.";
static const char* errninfo_str = "failed to turn on the swap space.\n\
        because there is no free swap_info in the swap_info_list.";

int main(int argc, char *argv[])
{
    int ret_value;
    const char* str_array[] = {erropen1_str, erropen2_str, errstat_str, 
                errgmem_str, errread_str, errnoswp_str, errszdif_str,
                errninfo_str};

    if (argc == 1) {
        fprintf(stderr, "swapon: arguments is too less.\n");
        return -1;
    }

    if (!(ret_value = syscall1(int, SYS_SWAPON, argv[1]))) 
        fprintf(stdout, "swapon: finish turning on the swap space!\n");
    else 
        fprintf(stdout, "swapon: %s\n", str_array[abs(ret_value + 10)]);
    return 0;
}