#include <xbook/lru.h>
#include <xbook/mutexlock.h>
#include <xbook/swap_stat.h>

/**
 * 把页描述符加入到LRU链表中的活跃链表
 */
void lru_cache_add(mem_range_t* range, struct page* page)
{
	mutex_lock(&range->lru_lock);
	if (!TestSetPageLRU(page))
		add_page_to_active_list(range, page);
		SetPageReferenced(page);
	mutex_unlock(&range->lru_lock);
}

/**
 * 从LRU链表中移除
 */
void lru_cache_del(mem_range_t* range, struct page* page)
{
	mutex_lock(&range->lru_lock);
	if (TestClearPageLRU(page)) {
		if (PageActive(page)) 
			del_page_from_active_list(range, page);
		else 
			del_page_from_inactive_list(range, page);
	} else 
		swapout_del(range, page);
	mutex_unlock(&range->lru_lock);
}

void swapout_del(mem_range_t* range, struct page* page)
{
	mutex_lock(&range->swapout_lock);
	if (PageDirty(page)) {
		remove_page_from_cache_unique(page);
		del_page_from_swapout_list(page);
	}
	mutex_unlock(&range->swapout_lock);
}


/**
 * 从LRU的不活跃链表移除，加入到活跃链表当中
 */
static inline void activate_page(mem_range_t* range, struct page* page)
{
	mutex_lock(&range->lru_lock);
	if (PageLRU(page) && !PageActive(page)) {
		del_page_from_inactive_list(range, page);
		add_page_to_active_list(range, page);
	}
	mutex_unlock(&range->lru_lock);
}

/**
 * 该页对应的描述符的flags的引用位被置位，且在LRU的不活跃链表当中，
 * 则把它加入到LRU的活跃链表内
 */
void mark_page_accessed(int range_type, struct page* page)
{
	if (PageLRU(page) && !PageActive(page) && PageReferenced(page)) {
		mem_range_t* range = range_gotten_by_type(range_type);
		activate_page(range, page);
	} else
		SetPageReferenced(page);
}
