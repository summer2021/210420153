#include <xbook/sharemem.h>
#include <xbook/memcache.h>
#include <xbook/debug.h>
#include <xbook/safety.h>
#include <xbook/memspace.h>
#include <xbook/semaphore.h>
#include <xbook/task.h>
#include <xbook/schedule.h>
#include <xbook/lru.h>
#include <xbook/mutexlock.h>
#include <sys/ipc.h>
#include <stddef.h>
#include <errno.h>
#include <string.h>

static share_mem_t *share_mem_table;

static rbtree_root id_root;
static rbtree_root paddr_root;
static mutexlock_t iroot_lock;
static mutexlock_t proot_lock;

DEFINE_SEMAPHORE(share_mem_mutex, 1);

/**
 * 在share_mem_table找一个空闲的shm，并设置赋予名字，设置大小
 * （以页单位）
 */
share_mem_t *share_mem_alloc(char *name, unsigned long size)
{
    share_mem_t *shm;
    for (int i = 0; i < MAX_SHARE_MEM_NR; i++) {
        shm = &share_mem_table[i];
        if (shm->name[0] == '\0') {
            if (!size)
                size = 1;
            size = PAGE_ALIGN(size);
            shm->npages = size / PAGE_SIZE;
            shm->page_addr = 0;
            shm->flags = 0;
            memcpy(shm->name, name, SHARE_MEM_NAME_LEN);
            shm->name[SHARE_MEM_NAME_LEN - 1] = '\0';

            mutex_lock(&iroot_lock);
            rbtree_insert(&shm->id_node, &id_root);
            mutex_unlock(&iroot_lock);

            return shm;
        }
    }
    return NULL;
}

int share_mem_free(share_mem_t *shm)
{
    unsigned long index, nr_pages, page_addr;

    if (shm->page_addr) {
        nr_pages = shm->npages;
        if (!(shm->flags & SHARE_MEM_PRIVATE)) {
            if (page_free(shm->page_addr))
                return -1;

            for (index = 0, page_addr = shm->page_addr; 
                 index < nr_pages; index++, page_addr += PAGE_SIZE)
                /*LRU_del：把物理页从LRU移除*/
                lru_del_by_range_type(MEM_RANGE_USER, page_addr);
        }
        
        for(index = 0, page_addr = shm->page_addr; 
            index < nr_pages; index++, page_addr += PAGE_SIZE)
            ClearPageReserved(GetMemMapIndex(page_addr));

        mutex_lock(&proot_lock);
        rbtree_delete(&shm->paddr_node, &paddr_root);
        shm->paddr_node.value = 0;
        mutex_unlock(&proot_lock);
    }

    mutex_lock(&iroot_lock);
    rbtree_delete(&shm->id_node, &id_root);
    mutex_unlock(&iroot_lock);

    memset(shm->name, 0, SHARE_MEM_NAME_LEN);
    return 0;
}

static share_mem_t *share_mem_find_by_name(char *name)
{
    share_mem_t *shm;
    int i;
    for (i = 0; i < MAX_SHARE_MEM_NR; i++) {
        shm = &share_mem_table[i];
        if (shm->name[0] != '\0') {
            if (!strcmp(shm->name, name)) 
                return shm;
        }
    }
    return NULL;
}

static share_mem_t *share_mem_find_by_id(int shmid)
{
    rbtree_node* node;
    share_mem_t* shm = NULL;

    mutex_lock(&iroot_lock);
    if ((node = find_node(&id_root, shmid)))
        shm = container_of(node, share_mem_t, id_node); 
    mutex_unlock(&iroot_lock);

    return shm;
}

share_mem_t *share_mem_find_by_addr(addr_t addr)
{
    rbtree_node* node;
    share_mem_t* shm = NULL;

    mutex_lock(&proot_lock);
    if ((node = find_node(&paddr_root, (int)addr)))
        shm = container_of(node, share_mem_t, paddr_node);
    mutex_unlock(&proot_lock);

    return shm;
}

/**
 * @flags: 获取标志
 *         IPC_CREAT: 如果共享内存不存在，则创建一个新的共享内存，否则就打开
 *         IPC_EXCL:  和CREAT一起使用，则要求创建一个新的共享内存，若已存在，就返回-1。
 *                    相当于在CREAT上面加了一个必须不存在的限定。
 * @return: 成功返回共享区域id，失败返回-1
 */
int share_mem_get(char *name, unsigned long size, unsigned long flags)
{
    if (name == NULL)
        return -1;
    if (size > 0 && PAGE_ALIGN(size) >= MAX_SHARE_MEM_SIZE)
        return -1;
    char craete_new = 0;
    int retval = -1;
    share_mem_t *shm;
    semaphore_down(&share_mem_mutex);
    if (flags & IPC_CREAT) {
        if (flags & IPC_EXCL) 
            craete_new = 1;
        
        if ((shm = share_mem_find_by_name(name))) {
            if (craete_new)
                goto err;
            retval = shm->id;
        } else {
            if (!(shm = share_mem_alloc(name, size)))
                goto err;
            retval = shm->id;
        }
    }
err:
    semaphore_up(&share_mem_mutex);
    return retval;
}

/**
 * @shmaddr: 共享内存的地址
 *          若该参数为NULL，则在进程空间自动选择一个闲的地址来映射，
 *          不为空，那么就在进程空间映射为该地址
 * 把共享内存的物理地址映射到当前进程的进程空间，
 * 需要用到的映射是虚拟地址和物理地址直接映射，不需要分配物理页，
 * 因为已经在分配共享内存时分配了物理页。
 * @return: 成功返回映射在进程空间的地址，失败返回-1
 */
void *share_mem_map(int shmid, void *shmaddr, int shmflg)
{
    share_mem_t *shm;
    semaphore_down(&share_mem_mutex);
    shm = share_mem_find_by_id(shmid);
    semaphore_up(&share_mem_mutex);
    if (shm == NULL) {
        errprint("shm %d not founded!" endl, shmid);
        return (void *) -1;
    }  

    unsigned long index, nr_pages, page_addr;
    task_t *cur = task_current;
    if (shmaddr == NULL) {
        if (!shm->page_addr) {
            shm->page_addr = page_alloc_user(shm->npages);
            if (!shm->page_addr)
                return (void *)-1;
            
            /* 把物理页对应的描述符加入到LRU当中 */
            nr_pages = shm->npages;
            page_addr = shm->page_addr;
            for (index = 0; index < nr_pages; index++, page_addr += PAGE_SIZE) {
                lru_add_by_range_type(MEM_RANGE_USER, page_addr);
                SetPageReserved(GetMemMapIndex(page_addr));
            }

            mutex_lock(&proot_lock);
            shm->paddr_node.value = shm->page_addr;
            rbtree_insert(&shm->paddr_node, &paddr_root);
            mutex_unlock(&proot_lock);
        }
        unsigned long flags = shmflg & IPC_REMAP ? 
                MEM_SPACE_MAP_REMAP | MEM_SPACE_MAP_SHARED : MEM_SPACE_MAP_SHARED;

        shmaddr = (void*)do_mem_space_map(cur->vmm, 0, shm->page_addr, 
                    shm->npages * PAGE_SIZE, PROT_USER | PROT_WRITE, flags);
    } else {
        unsigned long vaddr;
        if (shmflg & IPC_RND)
            vaddr = (unsigned long)shmaddr & PAGE_MASK;
        else 
            vaddr = (unsigned long)shmaddr;
        
        if (!shm->page_addr) {
            shm->page_addr = addr_vir2phy(vaddr);
            if (!shm->page_addr)
                return (void *)-1;
            shm->flags |= SHARE_MEM_PRIVATE;

            nr_pages = shm->npages;
            page_addr = shm->page_addr;
            for (index = 0; index < nr_pages; index++, page_addr += PAGE_SIZE) 
                SetPageReserved(GetMemMapIndex(page_addr));

            mutex_lock(&proot_lock);
            shm->paddr_node.value = shm->page_addr;
            rbtree_insert(&shm->paddr_node, &paddr_root);
            mutex_unlock(&proot_lock);
        }
        shmaddr = (void *)vaddr;
    }
    if (shmaddr != (void *)-1)
        atomic_inc(&shm->links);
    return shmaddr;
}

int share_mem_unmap(const void *shmaddr, int shmflg)
{
    if (!shmaddr) {
        return -1;
    }
    task_t *cur = task_current;
    unsigned long addr;
    if (shmflg & IPC_RND)
        addr = (unsigned long) shmaddr & PAGE_MASK;
    else 
        addr = (unsigned long) shmaddr;
    mem_space_t *sp = mem_space_find(cur->vmm, addr);
    if (sp == NULL) {
        keprint(PRINT_DEBUG "share_mem_unmap: not fond space\n");
        return -1;
    }
    addr = addr_vir2phy(addr);
    semaphore_down(&share_mem_mutex);
    share_mem_t *shm = share_mem_find_by_addr(addr);
    semaphore_up(&share_mem_mutex);
    
    /**
     * 如果要释放的共享空间不是用已有的空间作为共享空间，而是在
     * 映射空间的空闲空间设置的共享空间
     */
    int retval = 0;
    if (!(shm->flags & SHARE_MEM_PRIVATE)) 
        retval = do_mem_space_unmap(cur->vmm, sp->start, sp->end - sp->start);
    
    if (retval != -1) {
        if (shm) 
            atomic_dec(&shm->links);
    } else 
        keprint(PRINT_ERR "share_mem_unmap: do unmap at %x failed!\n", addr);

    return retval;
}

/** 
 * 把shm->link减一，如果小于等于0，则把物理地址空间释放
 */
int share_mem_put(int shmid)
{
    share_mem_t *shm;
    semaphore_down(&share_mem_mutex);
    if ((shm = share_mem_find_by_id(shmid))) {  
        if (atomic_get(&shm->links) <= 0) {
            share_mem_free(shm);
        } 
        semaphore_up(&share_mem_mutex);
        return 0;
    }
    semaphore_up(&share_mem_mutex);
    return -1;
}

/**
 * 把shm->link+1
 */
int share_mem_inc(int shmid)
{
    share_mem_t *shm;
    semaphore_down(&share_mem_mutex);
    if ((shm = share_mem_find_by_id(shmid))) {
        atomic_inc(&shm->links);
        semaphore_up(&share_mem_mutex);
        return 0;
    }
    semaphore_up(&share_mem_mutex);
    return -1;
}

/**
 * 把shm->link-1
 */
int share_mem_dec(int shmid)
{
    share_mem_t *shm;
    semaphore_down(&share_mem_mutex);
    if ((shm = share_mem_find_by_id(shmid))) {
        atomic_dec(&shm->links);
        semaphore_up(&share_mem_mutex);
        return 0;
    }
    semaphore_up(&share_mem_mutex);
    return -1;
}

int sys_shmem_get(char *name, unsigned long size, unsigned long flags)
{
    if (!name)
        return -EINVAL;
    if (mem_copy_from_user(NULL, name, SHARE_MEM_NAME_LEN) < 0)
        return -EINVAL;
    return share_mem_get(name, size, flags);
}

int sys_shmem_put(int shmid)
{
    return share_mem_put(shmid);
}

void *sys_shmem_map(int shmid, void *shmaddr, int shmflg)
{
    return share_mem_map(shmid, shmaddr, shmflg);
}

int sys_shmem_unmap(const void *shmaddr, int shmflg)
{
    /*if (mem_copy_from_user(NULL, (void *)shmaddr, PAGE_SIZE) < 0)
        return -EINVAL;*/
    return share_mem_unmap(shmaddr, shmflg);
}

void share_mem_init()
{
    share_mem_table = (share_mem_t *)mem_alloc(sizeof(share_mem_t) * MAX_SHARE_MEM_NR);
    if (!share_mem_table) /* must be ok! */
        panic(PRINT_EMERG "share_mem_init: alloc mem for share_mem_table failed! :(\n");

    init_rbtree(&id_root);
    init_rbtree(&paddr_root);
    mutexlock_init(&iroot_lock);
    mutexlock_init(&proot_lock);
    
    int i;
    for (i = 0; i < MAX_SHARE_MEM_NR; i++) {
        share_mem_table[i].id = 1 + i + i * 2; /* 共享内存id */
        share_mem_table[i].page_addr = 0;
        share_mem_table[i].npages = 0;
        share_mem_table[i].flags = 0;
        init_rbtree_node(&share_mem_table[i].id_node, share_mem_table[i].id, &id_root);
        init_rbtree_node(&share_mem_table[i].paddr_node, 0, &paddr_root);
        atomic_set(&share_mem_table[i].links, 0);
        memset(share_mem_table[i].name, 0, SHARE_MEM_NAME_LEN);
    }
}
