#include <xbook/fs.h>
#include <xbook/debug.h>
#include <xbook/memcache.h>
#include <xbook/swap.h>
#include <xbook/swap_stat.h>
#include <xbook/rb_tree.h>
#include <xbook/mutexlock.h>
#include <xbook/lru.h>
#include <xbook/mm.h>
#include <xbook/memspace.h>
#include <xbook/list.h>
#include <xbook/task.h>
#include <xbook/schedule.h>
#include <arch/mempool.h>
#include <arch/phymem.h>
#include <arch/page.h>
#include <string.h>
#include <unistd.h>
#include <stddef.h>

#define FIRST_INIT
//#define DEBUG_SWAP

#define DEF_NR_PAGES    32

#define IF_CONDITION(page) (!PageReserved(page) && !PageDirty(page))

char *swapfile_path = "/swapfile";

struct swap_info_struct swap_info[MAX_SWAPFILE];
mutexlock_t swapinfo_lock;
unsigned int used_swap_info = 0;

struct swapper_space swapper_space;

int page_cluster;

task_t* write_swap_thread;
task_t* kswapd_thread;

static inline void put_pages_inspace_to_list(unsigned long cluster, 
    unsigned long start_vaddr, unsigned long end_vaddr, unsigned int* max_mapped,
    mem_range_t* range, list_t **list, unsigned int* page_count)
{
    pde_t* pde;
    pte_t* pte;
    for (int i = 0; i < cluster && start_vaddr != end_vaddr && max_mapped; 
        i++, start_vaddr += PAGE_SIZE) 
    {
        pde = vir_addr_to_dir_entry(start_vaddr);
        pte = vir_addr_to_table_entry(start_vaddr);
        if ((*pde & PAGE_ATTR_PRESENT) && (*pte & PAGE_ATTR_PRESENT)) {
            struct page* page = GetMemMapIndex(addr_vir2phy(start_vaddr));
            if (IF_CONDITION(page) && PageLRU(page) && !PageActive(page)) {
                /**
                 * the page is moved from inactive_list to tmp_list
                 */
                ClearPageLRU(page);
	            del_page_from_inactive_list(range, page);
	            if (unlikely(!*list))
                    *list = &page->lru;
                else 
                    add_page_to_swapout_list(*list, page);
                (*page_count)++;
                (*max_mapped)--;
            }
        }
    }
}

static inline void seed_swap_init(struct seed_swap_struct* seed_swap, 
    unsigned long index_space, struct swap_entry_cluster* swp_cluster,
    int revmap_id)
{
    seed_swap->index_space = index_space;
    seed_swap->start_block = swp_cluster->index;
    seed_swap->nr_blocks = swp_cluster->count;
    seed_swap->revmap_id = revmap_id;
            
    list_init(&seed_swap->page_head);
    list_init(&seed_swap->sendswap_node);
            
    struct swap_info_struct* p = &swap_info[swp_cluster->type];
    init_rbtree_node(&seed_swap->rb_node, seed_swap->start_block, &p->sendswap_root);
    swap_sdev_lock(p);
    rbtree_insert(&seed_swap->rb_node, &p->sendswap_root);
    swap_sdev_unlock(p);
}

/**
 * try to swap page. first time max_scan inactive_list is 
 * examined from the head to the tail to look for the pages 
 * with no bit "Reserved", "Dirty". then they are put in 
 * the list, seed_swap_struct->page_head. wake up the kernel 
 * thread, write_swap_page to write these pages in the swap space.
 */
static int swap_out
(mem_range_t* range, unsigned int max_scan, unsigned int max_mapped)
{
    list_t* inactive_list = &range->inactive_list;
    list_t* entry = inactive_list->next;
    struct swap_entry_cluster* swp_cluster = 
        (struct swap_entry_cluster*)mem_alloc(sizeof(struct swap_entry_cluster) * (1<<page_cluster));
    if (!swp_cluster) 
        panic("there is no free memory to creat swp_cluster.");
    
    int stat = 0;
    mutex_lock(&range->lru_lock);
    while (max_scan-- && max_mapped && entry != inactive_list) 
    {
        struct page* page = GetPageFromLru(entry);
        unsigned int page_count = 0;

        entry = entry->next;

        if (IF_CONDITION(page)) 
        {
            /* 找出该页对应的反映射 */
            rev_mapping* revmap = page->rev_mapping;
            if (!find_node(&revmap_rbtree, revmap->rb_node.value))
                continue;
            /**
             * 从第一个space开始，因为物理页在共享空间内的偏移是一致的，所以
             * 在第一个space扫描不活跃匿名页就可以了 
             */
            mem_space_t* cur_space = GetSpaceFromNode(revmap->rev_map_node);
    #ifdef DEBUG_SWAP
            keprint(PRINT_WARING "start: %d\n, end: %d\n", cur_space->start, cur_space->end);
    #endif
            /* 设置在space内的起始扫描的位置 */
            unsigned long index_space = page->index & page_cluster;
            unsigned long vaddr = cur_space->start + index_space * PAGE_SIZE;
            /* space内结束的位置 */
            unsigned long end_vaddr = cur_space->end;
            /* 在space内扫描的范围，page_cluster在swap_setup()设置 */
            unsigned long cluster = 1 << page_cluster;
            list_t* tmp_list = NULL;
            struct seed_swap_struct* seed_swap;
            int i;
            
            /**
             * 把寄存器cr3的页目录表物理地址切换为该space对应的用户程序的
             * 页目录表的物理地址 
             */
            vmm_active(cur_space->vmm);
            /* 开始扫描 */
            put_pages_inspace_to_list(cluster, vaddr, end_vaddr, &max_mapped, 
                                      range, &tmp_list, &page_count);
            /* 切换回内核页目录表的物理地址 */
            vmm_active(NULL);

            if (page->mapping) {
                unsigned int type = SWP_TYPE(page->swp_entry);
                struct swap_info_struct* p = &swap_info[type];
                swap_sdev_lock(p);
                if (!(p->flags & SWAP_WRITE)) {
                    swap_sdev_unlock(p);
                    goto get_new_seed_swp;
                }

                rbtree_node* seed_swap_node = find_node_from_swapinfo(p, page->swp_entry);
                swap_sdev_unlock(p);
                if (unlikely(!seed_swap_node))
                    panic("swap_out: seed_swap hasn't been in swap_info!\n");
                seed_swap = GetSeedSwpFromRbn(seed_swap_node);

                unsigned long start_block = seed_swap->start_block;
                for (i = 0; i < seed_swap->nr_blocks; i++)
                    swap_duplicate(SWP_ENTRY((start_block+i), type));

                mutex_lock(&range->swapout_lock);
                list_add(&seed_swap->sendswap_node, &range->swap_out_list);
                range->nr_swapout_pages += seed_swap->nr_blocks;
                list_add_tail(&seed_swap->page_head, tmp_list);
                mutex_unlock(&range->swapout_lock);
                continue;
            }

    get_new_seed_swp:
            memset((void*)swp_cluster, 0, sizeof(struct swap_entry_cluster)*page_count);
            get_swap_block(page_count, swp_cluster);
            /**
             * usually just loop once
             */
            for (i = 0; swp_cluster[i].count; i++) {
                list_t* page_node = tmp_list;
                for (int j = 0; j < swp_cluster[i].count; j++, page_node = page_node->next) {
                    page = GetPageFromLru(page_node);
                    swap_entry_t swp_entry = 
                        SWP_ENTRY((swp_cluster[i].index + j), swp_cluster[i].type); 

                    if (add_to_swap_cache(page, swp_entry))
                        panic("swap out: the page has been in swap space!");
                }

                seed_swap = (struct seed_swap_struct*)mem_alloc(sizeof(struct seed_swap_struct));
                seed_swap_init(seed_swap, index_space, &swp_cluster[i], 
                                revmap->rb_node.value);
                
                mutex_lock(&range->swapout_lock);
                list_add(&seed_swap->sendswap_node, &range->swap_out_list);
                range->nr_swapout_pages += seed_swap->nr_blocks;
                
                list_add_tail(&seed_swap->page_head, tmp_list);
                if (unlikely(page_node != tmp_list)) {
                    /* the list is cut into two lists */
                    list_t* tmp = page_node->prev;
                    seed_swap->page_head.prev->next = page_node;
                    page_node->prev = seed_swap->page_head.prev;
                    tmp_list = page_node;

                    tmp->next = &seed_swap->page_head;
                    seed_swap->page_head.prev = tmp_list;
                }
                mutex_unlock(&range->swapout_lock);
            }
        stat = 1;
        }
    }
    mutex_unlock(&range->lru_lock);
    mem_free(swp_cluster);
    if (stat)
        task_wakeup(write_swap_thread);
    return 0;
}

/**
 * try to move the pages(struct page) from range->active_list 
 * to range->inactive_list
 */
static void refill_inactive
(mem_range_t* range, unsigned int nr_pages)
{
    list_t* active_list = &range->active_list;
    list_t* entry = active_list->prev;
    
    unsigned long flags;
	interrupt_save_and_disable(flags);	
    while (nr_pages && entry != active_list) {
        struct page* page = GetPageFromLru(entry);
        entry = entry->prev;
        /**
         * if the page has the bit "Referenced", it will be deleted and add 
         * to the head of the active_list again or it will be moved from 
         * active_list to inactive_list.
         */
        if (TestAndClearReferenced(page)) {
            list_del(&page->lru);
		    list_add(&page->lru, &range->active_list);
		    continue;
	    } 

        nr_pages--;

		del_page_from_active_list(range, page);
		add_page_to_inactive_list(range, page);
		SetPageReferenced(page);
    }
    interrupt_restore_state(flags);
}

/**
 * try to swap pages in inactive_list, so those pages aren't used 
 * by the CPU for a long time. they are put in the seed_swap_struct
 * . then the seed_swap_struct is put in the the swapout_list.
 */
void range_try_to_swap_pages(mem_range_t* range)
{
    unsigned int priority = range->priority;
    unsigned long nr_pages = DEF_NR_PAGES * (MAX_PRIORITY - priority);
    unsigned int ratio = nr_pages * range->nr_active_pages /
                         (range->nr_inactive_pages + 1) * 2;

    unsigned int max_scan, cur_max_scan,
                max_mapped, cur_max_mapped;         
    list_t* inactive_list = &range->inactive_list;

    max_scan = cur_max_scan = range->nr_inactive_pages / priority;
    if (max_scan < range->pages_low/2)
       return;

#ifdef DEBUG_SWAP
    keprint(PRINT_DEBUG "active\n");
    struct page* page;
    list_for_each_owner(page, &range->active_list, lru) {
        keprint(PRINT_INFO "active: paddr: 0x%x\n", GetPagePhyAdd(page));
        keprint(PRINT_WARING "active: id: %d\n", page->rev_mapping->rb_node.value);
    }
#endif
    refill_inactive(range, ratio);
#ifdef DEBUG_SWAP
    keprint(PRINT_DEBUG "inactive\n");
    list_for_each_owner(page, &range->inactive_list, lru) {
        keprint(PRINT_INFO "inactive: paddr: 0x%x\n", GetPagePhyAdd(page));
        keprint(PRINT_WARING "inactive: id: 0x%d\n", page->rev_mapping->rb_node.value);
    }
#endif
    max_mapped = cur_max_mapped = min((nr_pages << (7-priority)), max_scan / 10);

#ifdef DEBUG_SWAP
    keprint(PRINT_INFO "range_try_to_swap_pages: \n\
    nr_inactive_pages %d\n    nr_active_pages %d\n\
    priority %d\n    max_scan %d\n    max_mapped %d\n",
    range->nr_inactive_pages, range->nr_active_pages , priority, max_scan, max_mapped);
#endif

    list_t* entry;
    mutex_lock(&range->lru_lock);
    while (cur_max_scan-- && (entry = inactive_list->prev) != inactive_list) {
        struct page* page = GetPageFromLru(entry);

        list_del(entry);
        list_add(entry, inactive_list);
        if (IF_CONDITION(page)) {
            if (!cur_max_mapped--) {
                mutex_unlock(&range->lru_lock);
                swap_out(range, max_scan, max_mapped);
                return;
            }
        }
    }
    mutex_unlock(&range->lru_lock);
}

void do_swap_fault(unsigned long vaddr, swap_entry_t swap_entry, mem_space_t* space)
{
    unsigned long type = SWP_TYPE(swap_entry);
    unsigned long index = SWP_INDEX(swap_entry);
    struct swap_info_struct* p = &swap_info[type];

    struct page* page = find_page_by_index(swap_entry);
    swap_sdev_lock(p);
    rbtree_node* seed_swp_node = find_node_from_swapinfo(p, index);
    swap_sdev_unlock(p);
    if (!seed_swp_node) {
        pte_t* pte = vir_addr_to_table_entry(vaddr);
        if (!(*pte & PAGE_ATTR_PRESENT))
            panic("do_swap_fault: failed to look for seed swap with index in swap info as there is no it.\n");
    }
    struct seed_swap_struct* seed_swp = GetSeedSwpFromRbn(seed_swp_node);
    
    int read = 0;
    if (!page) {
        read = 1;
        if (alloc_page_to_read(seed_swp, type) == -1)
            panic("do_swap_fault: there is no free page in the user range.\n");
    }
        
    /**
     * the a page(struct page) isn't freed if it is in swap_cache and 
     * a bit PG_Launder of the page is set.
     */
    activate_swappage(type, seed_swp, space, read);
}

/**
 * init swapper_space
 */
static inline void swapper_space_init(void) 
{
    mutexlock_init(&swapper_space.list_lock);
    swapper_space.nr_pages = 0;
    radix_tree_init(&swapper_space.pfn_rtree);
    radix_tree_init(&swapper_space.index_rtree);
}

void swap_init() 
{
    keprint(PRINT_INFO "swap: starting swap.\n");
    mutexlock_init(&swapinfo_lock);
    mutexlock_init(&swaplist_lock);
    write_swap_thread = task_create
        ("write_swap", TASK_PRIO_LEVEL_NORMAL, write_swap_page, NULL);
#ifdef FIRST_INIT
    sys_mkswap(swapfile_path);
#endif
    sys_swapon(swapfile_path, (SWAP_FLAG_PRIO & SWAP_PRIO_MASK));
    swap_setup();
    swapper_space_init();

    keprint(PRINT_INFO "swap: init done.\n");
}

static void kswapd()
{
    mem_range_t* user_range = range_gotten_by_type(MEM_RANGE_USER);
    while (1) {
        if (user_range->free_pages < user_range->pages_high) 
            range_try_to_swap_pages(user_range);
        else
            task_sleep();
    }
}

void kswapd_init()
{
    keprint(PRINT_INFO "kswapd: starting kswapd.\n");
    kswapd_thread = task_create("kswapd", TASK_PRIO_LEVEL_NORMAL, kswapd, NULL);
    keprint(PRINT_INFO "kswapd: init done.\n");
}