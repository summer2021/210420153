#ifndef _X86_MEMPOOL_H
#define _X86_MEMPOOL_H

#include "phymem.h"
#include <xbook/list.h>
#include <xbook/memcache.h>
#include <xbook/mutexlock.h>
#include <const.h>

#define MEM_NODE_TYPE_DMA        0x01
#define MEM_NODE_TYPE_NORMAL     0x02
#define MEM_NODE_TYPE_USER      0x04

enum {
    MEM_RANGE_DMA = 0,
    MEM_RANGE_NORMAL,
    MEM_RANGE_USER,
    MEM_RANGE_NR
};

#define MEM_SECTION_MAX_NR      12
#define MEM_SECTION_MAX_SIZE    2048    // (2 ^ 11) : 8 MB


typedef struct {
    list_t free_list_head;
    size_t node_count;
    size_t section_size;
} mem_section_t;

typedef struct _mem_node {
    unsigned int count;
    unsigned int flags;         
    int reference; 
    list_t list;
    mem_section_t *section;
    mem_cache_t *cache;
    mem_group_t *group;
} mem_node_t;

/* 最大优先级 */
#define MAX_PRIORITY 7

typedef struct {
    unsigned int start; /* 区域的起始物理地址 */
    unsigned int end;   /* 区域的结束物理地址 */
    size_t pages;       /* 区域内的物理页的总数量 */
    /* 下三个字段为kswapd程序所用 */
    size_t free_pages;  /* 区域内的空闲物理页的总数量 */
    unsigned int pages_low;     /* 唤醒kswapd的阈值，当pages_low >= free_pages时，则表示区域
                                 * 内的空闲页紧缺，唤醒kswapd进行换出内存  */
    unsigned int pages_high;    /* 使kswaped睡眠的阈值，当pages_high >= free_pages时，则表示
                                 * 区域内空闲页充足，把kswaped阻塞，使其睡眠 */
    mem_section_t sections[MEM_SECTION_MAX_NR]; /* 与区域内的内存分配相关的字段 */
    mem_node_t *node_table;     /* 同上 */
    mutexlock_t lru_lock;       /* lru的互斥锁 */
    list_t active_list;         /* lru的活跃链表头部 */
    list_t inactive_list;       /* lru的不活跃链表头部 */
    unsigned int nr_active_pages;       /* lru的活跃链表的成员（物理页）数量 */
    unsigned int nr_inactive_pages;     /* lru的不活跃链表的成员（物理页）数量 */
    mutexlock_t swapout_lock;   /* 待换出链表的互斥锁 */
    list_t swap_out_list;       /* 待换出链表头部 */
    unsigned int nr_swapout_pages;      /* 待换出链表的成员（物理页）数量 */
    unsigned int priority;      /* 优先级，直接决定了在lru的不活跃链表扫描页的数量，间接决定了
                                 * 把活跃链表的页移动到不活跃链表的数量，以及需要被换出页的数量
                                 * ，优先级越大表示相应的数量变小（收缩内存的压力变小），优先级
                                 * 越小表示收缩内存的压力变大 */
} mem_range_t;

#define SIZEOF_MEM_NODE sizeof(mem_node_t) 

#define MEM_NODE_MARK_CHACHE_GROUP(node, _cache, _group)  \
        node->cache = _cache;                            \
        node->group = _group
        
#define MEM_NODE_MARK_SECTION(node, _section)  \
        node->section = _section
        
#define MEM_NODE_CLEAR_GROUP_CACHE(node)                \
        node->cache = NULL;                          \
        node->group = NULL
#define MEM_NODE_CLEAR_SECTION(node)                \
        node->section = NULL
        
#define MEM_NODE_GET_GROUP(node) node->group
#define MEM_NODE_GET_CACHE(node) node->cache
#define MEM_NODE_GET_SECTION(node) node->section

#define MEM_SECTION_DES_COUNT(section) \
        section->node_count--

#define MEM_SECTION_INC_COUNT(section) \
        section->node_count++

#define CHECK_MEM_NODE(node)                            \
        if (node == NULL) panic("Mem node error!\n") 

#define FREE_PAGES_SUB_COUNT(range, section) \
        range->free_pages -= section->section_size

#define FREE_PAGES_ADD_COUNT(range, section) \
        range->free_pages += section->section_size

#define SUB_PRIORITY(range)     \
do {                            \
    if (range->free_pages < (range->pages / MAX_PRIORITY * range->priority))\
        range->priority--;      \
} while(0);

#define ADD_PRIORITY(range)     \
do {                            \
    if (range->free_pages >= (range->pages / MAX_PRIORITY * (range->priority+1))) \
        range->priority++;      \
} while(0);

mem_node_t *phy_addr_to_mem_node(unsigned int page);
unsigned int mem_node_to_phy_addr(mem_node_t *node);

unsigned long mem_node_alloc_pages(unsigned long count, unsigned long flags);
unsigned long alloc_user_pages(unsigned long count);
int mem_node_free_pages(unsigned long page);

mem_range_t *mem_range_get_by_phy_addr(unsigned int addr);

void mem_range_init(unsigned int idx, unsigned int start, size_t len);

void mem_pool_test();

mem_range_t* range_gotten_by_type(int range_type);


#endif /*_X86_MEMPOOL_H */
