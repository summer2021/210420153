#include <xbook/swap_stat.h>
#include <xbook/rb_tree.h>
#include <xbook/swap.h>
#include <xbook/mm.h>

int swap_duplicate(swap_entry_t entry)
{
    unsigned int index = SWP_INDEX(entry);
    unsigned int type = SWP_TYPE(entry);
    int ret_val = 0;

    swap_sdev_lock(&swap_info[type]);
    if (index >= swap_info[type].nr_blocks && !swap_info[type].swap_map[index]) {
        ret_val = 1;
    } else {
        if (swap_info[type].swap_map[index] < SWAP_MAP_MAX - 1)
            swap_info[type].swap_map[index]++;
        else 
            swap_info[type].swap_map[index] = SWAP_MAP_MAX;
    }
    swap_sdev_unlock(&swap_info[type]);
    return ret_val;
}


int add_to_swap_cache(struct page* page, swap_entry_t entry) 
{
    if (page->mapping == &swapper_space) {
        keprint(PRINT_ERR "swap_cache: the page has been in swap_cache.\n");
        return -1;
    }
    if (swap_duplicate(entry)) {
        keprint(PRINT_ERR "swap_cache: the page has no block.\n");
        return -1;
    }
    if (add_page_to_cache_unique(page, entry)) {
        keprint(PRINT_ERR "swap_cache: the page has been in swap_cache too.\n");
        return -1;
    }
        
    return 0;
}

void swap_setup(void)
{
	unsigned long megs = mem_get_total_page_nr() >> (20 - PAGE_SHIFT);

	if (megs < 16)
		page_cluster = 2;
	else
		page_cluster = 3;
}

static rbtree_node* __find_node
(rbtree_node* node, rbtree_node* NIL, int value)
{
    if (node->value > value && node->left != NIL)
        return __find_node(node->left, NIL, value);

    struct seed_swap_struct* seed_swap = GetSeedSwpFromRbn(node);
    if ((node->value + seed_swap->nr_blocks-1) < value 
        && node->right != NIL)
        return __find_node(node->right, NIL, value);
    
    return node->value == value ? node : NULL;
}

rbtree_node* find_node_from_swapinfo
(struct swap_info_struct* p, unsigned long value)
{
    return __find_node(p->sendswap_root.root, &p->sendswap_root.NIL, value);
}