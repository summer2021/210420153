#include <xbook/rb_tree.h>
#include <xbook/debug.h>
#include <types.h>

//#define DEBUG_RBTREE

/** 
 * 红黑树性质：
 * 1.节点是红色或黑色。
 * 2.根是黑色。
 * 3.所有叶子都是黑色（叶子是空节点）。 
 * 4.每个红色节点必须有两个黑色的子节点。（从每个叶子到根的所有路径上
 *   不能有两个连续的红色节点。）
 * 5.从任一节点到其每个叶子的所有简单路径都包含相同数目的黑色节点。
 */

static inline rbtree_node* brother(rbtree_node* snode)
{
    if (!snode->parent)
        return NULL;
    
    return snode == snode->parent->left ? \
           snode->parent->right : snode->parent->left;
}

/**
 * 向左旋转，前提是node有子节点
 */
static void 
rotate_left(rbtree_node* node, rbtree_root* rbtree)
{
    rbtree_node* pnode = node->parent;

    if (pnode) {
        if (pnode->left == node) 
            pnode->left = node->right;
        else
            pnode->right = node->right;

        node->right->parent = pnode;
    } else {
        rbtree->root = node->right;
        node->right->parent = NULL;
    }
    
    node->parent = node->right;
    if ((node->right = node->parent->left) != &rbtree->NIL)
        node->right->parent = node;
    node->parent->left = node;
}

/**
 * 向右旋转，前提是node有子节点
 */
static void 
rotate_right(rbtree_node* node, rbtree_root* rbtree)
{
    rbtree_node* pnode = node->parent;

    if (pnode) {
        if (pnode->left == node)
            pnode->left = node->left;
        else
            pnode->right = node->left;

        node->left->parent = pnode;
    } else {
        rbtree->root = node->left;
        node->left->parent = NULL;
    }
        
    node->parent = node->left;
    if((node->left = node->parent->right) != &rbtree->NIL)
        node->left->parent = node;
    node->parent->right = node;
}

static inline void swap(rbtree_node* node1, rbtree_node* node2)
{
    rbtree_node tmp;

    tmp.color = node1->color;
    tmp.left = node1->left;
    tmp.right = node1->right;
    tmp.parent = node1->parent;

    node1->color = node2->color;
    node1->left = node2->left;
    node1->right = node2->right;
    node1->parent = node2->parent;

    node2->color = tmp.color;
    node2->left = tmp.left;
    node2->right = tmp.right;
    node2->parent = tmp.parent;
}

/**
 * 从上往下，递归式查找最小值的节点，如果找到当前节点的左子节点为空，则
 * 停止递归，并返回该节点。
 * 真正的目的是找到最多只有一个子节点的父节点
 */
rbtree_node* 
get_smallest_node(rbtree_node* node, rbtree_root* rbtree)
{
    return node->left != &rbtree->NIL ? 
           get_smallest_node(node->left, rbtree) : node;
}

/**
 * 插入一个新节点后，开始调整红黑树 
 */
static void insert_case(rbtree_node* node, rbtree_root* rbtree)
{
    rbtree_node *unode, *gnode;

    /**
     * 情形1：node无父节点，说明node是根节点，设为黑色并返回
     */
    if (!node->parent) {
        node->color = RB_BLACK;
        return;
    }

    /**
     * 情形2：如果node的父节点是黑色，没有破坏性质4和性质5,所以直接返回
     */
    if (node->parent->color == RB_RED) 
    {
        gnode = node->parent && node->parent->parent ? \
                node->parent->parent : NULL;
        
        if (gnode)
            unode = node->parent == gnode->left ? \
                    gnode->right : gnode->left;
        else 
            unode = NULL;
        
        /**
         * 情形3：node、父节点和叔父节点都是红色的，破坏了性质4
         */
        if (unode && unode->color == RB_RED) 
        {
            node->parent->color = unode->color = RB_BLACK;
            gnode->color = RB_RED;
            /**
             * 如果爷节点是根节点，而且为红色，所以破坏了性质2，所以
             * 向上调整红黑树
             */
            insert_case(gnode, rbtree);
        } 
        else 
        {
            /**
             * 情形4：node节点和父节点的颜色为红色，这破坏了性质4。
             * 所以当node无子节点，且存在兄弟节点以及叔父节点时（都是黑色），
             * 把node设为黑色即可。
             * 如果以上条件不满足，则使用左右旋转调整红黑树
             */
            if (node->left == &rbtree->NIL && 
                node->right == &rbtree->NIL && 
                brother(node) && unode && 
                unode != &rbtree->NIL) 
                node->color = RB_BLACK;
            else if (gnode)
            {
                if (node->parent->left == node) 
                {
                    if (gnode->left == node->parent) 
                    {
                        node->parent->color = RB_BLACK;
                        gnode->color = RB_RED;
                        rotate_right(gnode, rbtree);
                    } 
                    else 
                    {
                        rotate_right(node->parent, rbtree);
                        node->color = RB_BLACK;
                        node->parent->color = RB_RED;
                        rotate_left(node->parent, rbtree);
                    }
                } 
                else
                {
                    if (gnode->left == node->parent) 
                    {
                        rotate_left(node->parent, rbtree);
                        node->color = RB_BLACK;
                        node->parent->color = RB_RED;
                        rotate_right(node->parent, rbtree);
                    } 
                    else 
                    {
                        node->parent->color = RB_BLACK;
                        gnode->color = RB_RED;
                        rotate_left(gnode, rbtree); 
                    }
                }
            }
        }
    }
}

/**
 * 在红黑树插入一个新节点，前提是有root节点才能执行
 * 以root为开头，从上往下、以递归、判断大小的方式查找到合适的位置，
 * 再新建节点并加入，然后开始调整红黑树
 */
static void __insert
(rbtree_node* node, rbtree_node* new_node, rbtree_root* rbtree)
{
    if (node->value >= new_node->value) {
        if (node->left != &rbtree->NIL)
            __insert(node->left, new_node, rbtree);
        else {
            node->left = new_node;
            new_node->parent = node;
            insert_case(new_node, rbtree);
            return;
        }
    } else {
        if (node->right != &rbtree->NIL)
            __insert(node->right, new_node, rbtree);
        else {
            node->right = new_node;
            new_node->parent = node;
            insert_case(new_node, rbtree);
            return;
        }     
    }
}

/**
 * 函数接口：在红黑树插入一个新节点
 * （PS：先给new_node初始化了再调用该函数）
 */
void rbtree_insert(rbtree_node* new_node, rbtree_root* rbtree) 
{
    if (!rbtree->root) {
        new_node->color = RB_BLACK;
        new_node->parent = NULL;
        new_node->left = \
        new_node->right = &rbtree->NIL;
        rbtree->root = new_node;
    } else {
        __insert(rbtree->root, new_node, rbtree);
    }
}

/** 
 * 删除节点后违反性质5就调用该函数
 */
void delete_case(rbtree_node* node, rbtree_root* rbtree)
{
    /**
     * 情景1：如节点node为根节点，则设置颜色为黑色并返回
     */
    if (!node->parent) {
        node->color = RB_BLACK;
        return;
    }

    rbtree_node* bnode = brother(node);

    /**
     * 情景2：如果兄弟节点存在且为红色，则说明他的父节点以及子节点都是黑
     * 色。所以变色，然后根据节点node向左/右旋转。
     * 完成后还未使其平衡，因为父节点经过兄弟节点到叶节点的路径数量和父节
     * 点经过node到叶节点的路径数量不相等，相比之下node还缺少一个黑节点，
     * 即未满足性质5。还需到情景4、5或6进行处理。
     */
    if (bnode->color == RB_RED) 
    {
        node->parent->color = RB_RED;
        bnode->color = RB_BLACK;
        if (node == node->parent->left)
            rotate_left(node->parent, rbtree);
        else 
            rotate_right(node->parent, rbtree);
    }
    else
    {
        if (bnode->left->color == RB_BLACK &&
            bnode->right->color == RB_BLACK)
        {   
            /** 
             * 情景3：如果node、父节点、兄弟节点和兄弟节点的两个子节都是
             * 黑色，则把兄弟节点设为黑色。
             * 但是依旧违反性质5，因为node的爷节点到任一叶节点的路径必须
             * 相同，即叔父节点以下到任一叶节点的路径和node的父节点以下到
             * 任一叶节点不相同，所以以父节点为起点，往上递归调整红黑树
             */
            if (node->parent->color == RB_BLACK)
            {
                bnode->color = RB_RED;
                delete_case(node->parent, rbtree);
            }
            /**
             * 情景4：如果node、兄弟节点和兄弟节点的两个子节为黑色，父节点
             * 为红色。则把兄弟节点设为红色，父节点为黑色
             */
            else if (node->parent->color == RB_RED)
            {
                bnode->color = RB_RED;
                node->parent->color = RB_BLACK;
            }
        } 
        else 
        {
            /* 情景5 */
            if (bnode->color == RB_BLACK)
            {
                if (node == node->parent->left && 
                    bnode->left->color == RB_RED &&
                    bnode->right->color == RB_BLACK)
                {
                    bnode->color = RB_RED;
                    bnode->left->color = RB_BLACK;
                    rotate_right(bnode, rbtree);
                } 
                else if (node == node->parent->right &&
                        bnode->left->color == RB_BLACK &&
                        bnode->right->color == RB_RED)
                {
                    bnode->color = RB_RED;
                    bnode->right->color = RB_BLACK;
                    rotate_left(bnode, rbtree);
                }
            }

            /* 情景6 */
            bnode->color = node->parent->color;
            node->parent->color = RB_BLACK;
            if (node == node->parent->left)
            {
                bnode->right->color = RB_BLACK;
                rotate_left(node->parent, rbtree);
            } 
            else 
            {
                bnode->left->color = RB_BLACK;
                rotate_right(node->parent, rbtree);
            }
        }
    }
}

/**
 * 删除节点
 * 如node有两个子节点则调用函数getSmallestChild(node->right)开始从
 * 上往下查找相对小的值,且最多只有一个非NIL子节点的节点，然后把node和相
 * 对小值的节点调换，把node调用函数delete_one_child()进行删除。
 * 
 * 其中“调用函数getSmallestChild()上往下查找相对小的值,且最多只有一个
 * 非NIL子节点的节点，把等于data的节点和相对小值节点的值调换，再释放它。
 * ” 这个主要目的是，如果要释放的节点有两个子节点，那么释放它后，那两个子
 * 节点取其中一个补位，但是这个被补位的子节点它也有非NIL子节点，那么进行
 * 调整需要很多的时间，如果非NIL子节点位于红黑树的上方更是如此，更加的繁
 * 琐。所以在节点右边从上往下找出相对小值，且最多只有一个非NIL子节点的节
 * 点，并替换值，那么要释放的节点在红黑树相对下方，且最多只有一个非NIL子
 * 节点，调整红黑树也相对快很多。
 */
rbtree_node* rbtree_delete(rbtree_node* node, rbtree_root* rbtree)
{
    rbtree_node *smallest_node = NULL, *child = NULL;

    if (node->left != &rbtree->NIL &&
        node->right != &rbtree->NIL) 
    {
        smallest_node = get_smallest_node(node->right, rbtree);
        /**
         * 如果smallest_node是node的子节点，把node右子节点指向自己，
         * 否则替换后smallest_node右子节点还是自己的节点，而不是node
         * ；把smallest_node的父节点指向自己，否则替换后node的父节点
         * 还是自己
         * before:                  after:
         *       O node                   O smallest_node
         *      / \                      / \
         *     O   O smallest_node      O   O node
         */
        if (smallest_node->parent == node) {
            node->right = node;
            smallest_node->parent = smallest_node;
        }
            
        if (node == rbtree->root)
            rbtree->root = smallest_node;
        
        swap(node, smallest_node);
    } 
    else if (node == rbtree->root) 
    {
        if (node->left == &rbtree->NIL && 
            node->right == &rbtree->NIL) {
            rbtree->root = NULL;
        } else {
            child = node->left != &rbtree->NIL ? 
                    node->left : node->right;
            rbtree->root = child;
            child->parent = NULL;
            child->color = RB_BLACK;
        }
        return child;
    }

    child = node->left != &rbtree->NIL ? 
            node->left : node->right;

    if (node->parent->left == node)
        node->parent->left = child;
    else 
        node->parent->right = child;
    child->parent = node->parent;
    
    /**
     * 开始调整，这里是重点
     * 1、如果节点node的颜色是红色的，则表示它的父节点以及子节点的颜色都
     * 是黑色，那么子节点代替节点node上位之后并不影响红黑树的性质。
     * 2、如果节点node的颜色是黑色的，则它的父节点可能是黑色或红色。如果
     * 子节点是红色，当子节点代替节点node上位之后，那么会违反性质5，如果
     * 父节点是红色的，会违反性质4。把该节点的颜色设为黑色。
     * 3、如果节点node的颜色是黑色的，则它的父节点可能是黑色或红色。如果
     * 子节点是黑色的，当子节点代替节点P上位之后，会违反性质5（从任一节点
     * 到其每个叶子的所有路径都包含相同数目的黑色节点）。所以调用函数del-
     * ete_case()调整子节点下方的红黑子树。
     */
    if (node->color == RB_BLACK) {
        if (child->color == RB_RED)
            child->color = RB_BLACK;
        else 
            delete_case(child, rbtree);
    }

    return smallest_node ? smallest_node : child;
}

static rbtree_node* __find_node
(rbtree_node* node, rbtree_node* NIL, int value)
{
    if (node->value > value && node->left != NIL)
        return __find_node(node->left, NIL, value);
    else if (node->value < value && node->right != NIL)
        return __find_node(node->right, NIL, value);
   
    return node->value == value ? node : NULL;
}

inline rbtree_node* find_node(rbtree_root* rbtree, int value)
{
    return __find_node(rbtree->root, &rbtree->NIL, value);
}

/* 用来测试的 */
/*
void rbtree_test()
{
    rbtree_root root;
    rbtree_node node_array[50], *node;
    init_rbtree(&root);

    int i,j;
    for (i = 0; i < 50; i++) {
        init_rbtree_node(&node_array[i], i, &root);
        rbtree_insert(&node_array[i], &root);
    }

    for (i = 0; i < 50; i++) {
        node = find_node(root.root, &root, i);
        if (node)
            keprint(PRINT_INFO "the value in the node: %d\n", node->value);
        else
            keprint(PRINT_ERR "the value in the node was null!\n");
    }

    for (i = 0; i < 50; i++) {
        keprint("the root:%d\n", root.root->value);
        rbtree_delete(&node_array[i], &root);
    }
}
*/